<?php
include "functions/database.php";
$query = $db->query("SELECT * FROM settings");

$result = $db->fetch_array($query);
$prv = $result['prv'];
if ($prv) {
  session_start();
  if (empty($result['redirect']) && empty($_SESSION["valid_user"])) {
    $db->close_connection();
    Header("Location: admin/login.php");
    exit;
  } elseif (empty($_SESSION["valid_user"])) {
    $db->close_connection();
    Header("Location: " . $result["redirect"]);
    exit;
  }
}
$name = $result['name'];
$URL = $result['URL'];
$logo = $result['logo'];
$des = $result['des'];

include "functions/functions.php";

$countriesTrans = array('AF' => 'Afghanistan','AX' => 'Aland Islands','AL' => 'Albania','DZ' => 'Algeria','AS' => 'American Samoa','AD' => 'Andorra','AO' => 'Angola','AI' => 'Anguilla','AQ' => 'Antarctica','AG' => 'Antigua And Barbuda','AR' => 'Argentina','AM' => 'Armenia','AW' => 'Aruba','AU' => 'Australia','AT' => 'Austria','AZ' => 'Azerbaijan','BS' => 'Bahamas','BH' => 'Bahrain','BD' => 'Bangladesh','BB' => 'Barbados','BY' => 'Belarus','BE' => 'Belgium','BZ' => 'Belize','BJ' => 'Benin','BM' => 'Bermuda','BT' => 'Bhutan','BO' => 'Bolivia','BA' => 'Bosnia And Herzegovina','BW' => 'Botswana','BV' => 'Bouvet Island','BR' => 'Brazil','IO' => 'British Indian Ocean Territory','BN' => 'Brunei Darussalam','BG' => 'Bulgaria','BF' => 'Burkina Faso','BI' => 'Burundi','KH' => 'Cambodia','CM' => 'Cameroon','CA' => 'Canada','CV' => 'Cape Verde','KY' => 'Cayman Islands','CF' => 'Central African Republic','TD' => 'Chad','CL' => 'Chile','CN' => 'China','CX' => 'Christmas Island','CC' => 'Cocos (Keeling) Islands','CO' => 'Colombia','KM' => 'Comoros','CG' => 'Congo','CD' => 'Congo, Democratic Republic','CK' => 'Cook Islands','CR' => 'Costa Rica','CI' => 'Cote D\'Ivoire','HR' => 'Croatia','CU' => 'Cuba','CY' => 'Cyprus','CZ' => 'Czech Republic','DK' => 'Denmark','DJ' => 'Djibouti','DM' => 'Dominica','DO' => 'Dominican Republic','EC' => 'Ecuador','EG' => 'Egypt','SV' => 'El Salvador','GQ' => 'Equatorial Guinea','ER' => 'Eritrea','EE' => 'Estonia','ET' => 'Ethiopia','FK' => 'Falkland Islands (Malvinas)','FO' => 'Faroe Islands','FJ' => 'Fiji','FI' => 'Finland','FR' => 'France','GF' => 'French Guiana','PF' => 'French Polynesia','TF' => 'French Southern Territories','GA' => 'Gabon','GM' => 'Gambia','GE' => 'Georgia','DE' => 'Germany','GH' => 'Ghana','GI' => 'Gibraltar','GR' => 'Greece','GL' => 'Greenland','GD' => 'Grenada','GP' => 'Guadeloupe','GU' => 'Guam','GT' => 'Guatemala','GG' => 'Guernsey','GN' => 'Guinea','GW' => 'Guinea-Bissau','GY' => 'Guyana','HT' => 'Haiti','HM' => 'Heard Island & Mcdonald Islands','VA' => 'Holy See (Vatican City State)','HN' => 'Honduras','HK' => 'Hong Kong','HU' => 'Hungary','IS' => 'Iceland','IN' => 'India','ID' => 'Indonesia','IR' => 'Iran, Islamic Republic Of','IQ' => 'Iraq','IE' => 'Ireland','IM' => 'Isle Of Man','IL' => 'Israel','IT' => 'Italy','JM' => 'Jamaica','JP' => 'Japan','JE' => 'Jersey','JO' => 'Jordan','KZ' => 'Kazakhstan','KE' => 'Kenya','KI' => 'Kiribati','KR' => 'Korea','KW' => 'Kuwait','KG' => 'Kyrgyzstan','LA' => 'Lao People\'s Democratic Republic','LV' => 'Latvia','LB' => 'Lebanon','LS' => 'Lesotho','LR' => 'Liberia','LY' => 'Libyan Arab Jamahiriya','LI' => 'Liechtenstein','LT' => 'Lithuania','LU' => 'Luxembourg','MO' => 'Macao','MK' => 'Macedonia','MG' => 'Madagascar','MW' => 'Malawi','MY' => 'Malaysia','MV' => 'Maldives','ML' => 'Mali','MT' => 'Malta','MH' => 'Marshall Islands','MQ' => 'Martinique','MR' => 'Mauritania','MU' => 'Mauritius','YT' => 'Mayotte','MX' => 'Mexico','FM' => 'Micronesia, Federated States Of','MD' => 'Moldova','MC' => 'Monaco','MN' => 'Mongolia','ME' => 'Montenegro','MS' => 'Montserrat','MA' => 'Morocco','MZ' => 'Mozambique','MM' => 'Myanmar','NA' => 'Namibia','NR' => 'Nauru','NP' => 'Nepal','NL' => 'Netherlands','AN' => 'Netherlands Antilles','NC' => 'New Caledonia','NZ' => 'New Zealand','NI' => 'Nicaragua','NE' => 'Niger','NG' => 'Nigeria','NU' => 'Niue','NF' => 'Norfolk Island','MP' => 'Northern Mariana Islands','NO' => 'Norway','OM' => 'Oman','PK' => 'Pakistan','PW' => 'Palau','PS' => 'Palestinian Territory, Occupied','PA' => 'Panama','PG' => 'Papua New Guinea','PY' => 'Paraguay','PE' => 'Peru','PH' => 'Philippines','PN' => 'Pitcairn','PL' => 'Poland','PT' => 'Portugal','PR' => 'Puerto Rico','QA' => 'Qatar','RE' => 'Reunion','RO' => 'Romania','RU' => 'Russian Federation','RW' => 'Rwanda','BL' => 'Saint Barthelemy','SH' => 'Saint Helena','KN' => 'Saint Kitts And Nevis','LC' => 'Saint Lucia','MF' => 'Saint Martin','PM' => 'Saint Pierre And Miquelon','VC' => 'Saint Vincent And Grenadines','WS' => 'Samoa','SM' => 'San Marino','ST' => 'Sao Tome And Principe','SA' => 'Saudi Arabia','SN' => 'Senegal','RS' => 'Serbia','SC' => 'Seychelles','SL' => 'Sierra Leone','SG' => 'Singapore','SK' => 'Slovakia','SI' => 'Slovenia','SB' => 'Solomon Islands','SO' => 'Somalia','ZA' => 'South Africa','GS' => 'South Georgia And Sandwich Isl.','ES' => 'Spain','LK' => 'Sri Lanka','SD' => 'Sudan','SR' => 'Suriname','SJ' => 'Svalbard And Jan Mayen','SZ' => 'Swaziland','SE' => 'Sweden','CH' => 'Switzerland','SY' => 'Syrian Arab Republic','TW' => 'Taiwan','TJ' => 'Tajikistan','TZ' => 'Tanzania','TH' => 'Thailand','TL' => 'Timor-Leste','TG' => 'Togo','TK' => 'Tokelau','TO' => 'Tonga','TT' => 'Trinidad And Tobago','TN' => 'Tunisia','TR' => 'Turkey','TM' => 'Turkmenistan','TC' => 'Turks And Caicos Islands','TV' => 'Tuvalu','UG' => 'Uganda','UA' => 'Ukraine','AE' => 'United Arab Emirates','GB' => 'United Kingdom','US' => 'United States','UM' => 'United States Outlying Islands','UY' => 'Uruguay','UZ' => 'Uzbekistan','VU' => 'Vanuatu','VE' => 'Venezuela','VN' => 'Viet Nam','VG' => 'Virgin Islands, British','VI' => 'Virgin Islands, U.S.','WF' => 'Wallis And Futuna','EH' => 'Western Sahara','YE' => 'Yemen','ZM' => 'Zambia','ZW' => 'Zimbabwe',);
$id = preg_replace("/[^a-z0-9.]+/i", '', $_GET['id']);
$querry = "SELECT * FROM links WHERE BINARY stats='$id'";
$result = $db->query($querry);
$result = $db->fetch_array($result);
$link = $db->escape_value($result["link"]);
$db->close_connection();
$worldData = $result["statsdata"];
$referers = $result["refererdata"];
if(!$result["stats"] == "") {
  if(!$worldData == ""){
    $noWorldData = 0;
    $worldData = strToArr($worldData);
    $worldjsData = [];
    for ($i=0; $i < count($worldData[0]); $i++) { 
      $contryCode = $worldData[1][$i];
      $contry = $countriesTrans[$contryCode];
      $rank = $worldData[0][$i];
      $hitsperc = round((($rank / $result["hits"]) * 100), 2);
      $sample = "data.addRows([[{v:'".$contryCode."',f:'".$contry."'},".$rank.",'".$hitsperc."% of Visits']]);ivalue['".$contryCode."'] = '';";
      array_push($worldjsData, $sample);
    };
    $worldjsData = implode(" ", $worldjsData);
    $sortedworldDataa = [];
    $sortworldData = $worldData[0];
    rsort($sortworldData);
    $sortedworldDatapos = [];
    foreach ($sortworldData as $value) {
      $pos = array_search($value, $worldData[0]);
      if(!in_array($pos, $sortedworldDatapos)){
        array_push($sortedworldDatapos, $pos);
      } else {
        for ($r=0; $r < count($worldData[0]); $r++) { 
          if(($value == $worldData[0][$r]) && !($r == $pos)) {
            $pos = $r;
            if(!in_array($r,$sortedworldDatapos)){
              array_push($sortedworldDatapos, $pos);
            };
          };
        };
      };
      if(count($sortedworldDatapos) == 5){break;}
    };
    for ($i=0; $i < count($sortedworldDatapos); $i++) { 
      if ($worldData[0][$sortedworldDatapos[$i]] == 1){
        $send = "<p class=\"label ref-label\"><span class=\"ref-one\">".$worldData[0][$sortedworldDatapos[$i]]." visit</span> from ".$countriesTrans[$worldData[1][$sortedworldDatapos[$i]]]."</p><br>";
      } else {
        $send = "<p class=\"label ref-label\"><span class=\"ref-one\">".$worldData[0][$sortedworldDatapos[$i]]." visits</span> from ".$countriesTrans[$worldData[1][$sortedworldDatapos[$i]]]."</p><br>";
      }
    array_push($sortedworldDataa, $send);
    };
    $sortedworldDataa = implode(" ", $sortedworldDataa);
  } else {
    $noWorldData = 1;
  }
  ;
  if(!$referers == ""){
    $noreferers = 0;
    $referers = strToArr($referers);
    $refererdata = $referers[0];
    $refererdatapos = [];
    rsort($refererdata);
    foreach ($refererdata as $value) {
      $poso = array_search($value, $referers[0]);
      if(!in_array($poso, $refererdatapos)){
        array_push($refererdatapos, $poso);
      } else {
        for ($r=0; $r < count($referers[0]); $r++) { 
          if(($value == $referers[0]) && !($r == $poso)) {
            $poso = $r;
            if(!in_array($r,$refererdatapos)){
              array_push($refererdatapos, $poso);
            };
          };
        };
      };
      if(count($refererdatapos) == 5){break;}

    };
    $refererdata = [];
    for ($i=0; $i < count($refererdatapos); $i++) { 
      if ($referers[0][$refererdatapos[$i]] == 1){
        $send = "<p class='label ref-label'> <span class='ref-one'>{$referers[0][$refererdatapos[$i]]} referral</span> {$referers[1][$refererdatapos[$i]]}</p><br>";
      } else {
        $send = "<p class='label ref-label'> <span class='ref-one'>{$referers[0][$refererdatapos[$i]]} referrals</span> {$referers[1][$refererdatapos[$i]]}</p><br>";
      }
      array_push($refererdata, $send);
    };
    $refererdata = implode(" ", $refererdata);
  } else {
    $noreferers = 1;
  }
} else {
   header('Location: ' . $URL . '/404');
  exit;
}
?>
<!doctype html>
<html lang="en" class="no-js">
  <head>
    <meta charset="utf-8">
<base href="<?php echo $URL;?>/">


    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <title><?php echo $name;?> - stats</title>
    <meta name="description" content="<?php echo $des;?>">

    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />

    <!--- Font-Awesome CDN -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!--- Core CSS-->
    <link href="css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
    <link href="css/normalize.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/alertify.core.css" />
    <link rel="stylesheet" href="css/alertify.bootstrap.css" />
    
    <!-- Custom CSS -->
    <link href="css/style.css" media="all" rel="stylesheet" type="text/css" />

    <!-- Animate.css -->
    <link href="css/animate.css" rel="stylesheet" type="text/css"/>

    <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script src="http://gregfranko.com/jquery.selectBoxIt.js/js/jquery.selectBoxIt.min.js"></script>
    <script src="js/stylefill.js" type="text/javascript"></script>
    <script src="js/bigtext.js" type="text/javascript"></script>    
    <script src="js/jquery.dotdotdot.js" type="text/javascript"></script>

    
  </head>

  <body class="home-bg">
    <div class="container">
       <a href="<?php echo $URL;?>"><img alt="Logo" class="title-logo center-block small" src="<?php echo $logo;?>"/></a>
      <div class="row" style="margin-top:40px;">
        <div class="col-md-6 col-md-offset-3 col-xs-12 more">
          <p class="input-label label-truncate">Description: <?php echo $result["text"];?></p>
          <input type="text" name="shortURL" readonly class="form-control url-field anim" autocomplete="off" id="short" value='<?php echo $URL.'/'.$result["short"];?>'></input>
          <div class="col-xs-3 text-center">
            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $URL.'/'.$result["short"];?>"><button class="btn social-btn anim mrgn-top fb"><i class="ion-social-facebook"></i></button></a>
          </div>
          <div class="col-xs-3 text-center">
            <a target="_blank" href="https://twitter.com/home?status=<?php echo $URL.'/'.$result["short"];?>"><button class="btn social-btn anim mrgn-top twit"><i class="ion-social-twitter"></i></button></a>
          </div>
          <div class="col-xs-3 text-center">
            <a target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php echo $URL.'/'.$result["short"];?>"><button class="btn social-btn anim mrgn-top pinterest"><i class="ion-social-pinterest"></i></button></a>
          </div>
          <div class="col-xs-3 text-center">
            <a target="_blank" href="https://plus.google.com/share?url=<?php echo $URL.'/'.$result["short"];?>"><button class="btn social-btn anim mrgn-top gplus"><i class="ion-social-googleplus"></i></button></a>
          </div>
          <div id="vis-container" class="col-xs-12">
            <div id='visualization' class="center-block"></div>
          </div>
          <hr>
          <?php if(!$noWorldData) {?>
            <?php if(!$noreferers) { ?>
            <div class="col-xs-6 center-block sites-ref">
                <p class="ref-title"><i class="ion-arrow-graph-up-right"></i> Top Referrals</p>
                <?php echo $refererdata; ?>
              </div>
            <?php }; ?>
              <div class="col-xs-6 center-block countries-ref">
                <p class="ref-title"><i class="ion-ios-world"></i> Top Countries</p>
                 <?php echo $sortedworldDataa; ?> 
              </div>
          
            <br>
          <?php }; ?>
          <div class="col-lg-12">
            <div class="col-xs-12 col-md-6 col-lg-6 col-sm-12 text-center hits">
              <p class="info-title text-center">Button hits</p>
              <br>
              <div class="info-num center-block btn-hits"><?php echo $result["clicks"]; ?></div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 col-sm-12 text-center hits">
              <p class="info-title text-center">Total hits</p>
              <br>
              <div class="info-num center-block tot-hits"><?php echo $result["hits"]; ?></div>
            </div>
          </div>
          <div class="ratio-div col-xs-6 col-xs-offset-3 anim-slow">
            <p class="ratio-text">1 button hit for every 1 hits</p>
          </div>
        </div>
      </div>
    </div>







  <script type='text/javascript' src='http://www.google.com/jsapi'></script>
  <script src="js/alertify.min.js"></script>

  <script>
    $(document).on('ready resize', function(){
      var winWidth = $(window).width();
    });
  </script>
  <script type='text/javascript'>google.load('visualization', '1', {'packages': ['geochart']});
    //google.setOnLoadCallback(drawVisualization);

      function drawVisualization() {var data = new google.visualization.DataTable();

     data.addColumn('string', 'Country');
     data.addColumn('number', 'Value'); 
     data.addColumn({type:'string', role:'tooltip'});var ivalue = new Array();

     <?php echo $worldjsData; ?>

     var options = {
     backgroundColor: {fill:'#FFFFFF',stroke:'#FFFFFF' ,strokeWidth:0},
     colorAxis:  {minValue: 0,  colors: ['#c2ced3','#00628c']},
     legend: 'none',  
     backgroundColor: {fill:'transparent',stroke:'#FFFFFF' ,strokeWidth:0 },  
     datalessRegionColor: '#eaeaea',
     displayMode: 'regions', 
     enableRegionInteractivity: 'true', 
     resolution: 'countries',
     sizeAxis: {minValue: 1, maxValue:1,minSize:10,  maxSize: 10},
     region:'world',
     keepAspectRatio: true,
     width: document.getElementById('vis-container').innerWidth ,
     tooltip: {textStyle: {color: '#444444'}, trigger:'focus'}  
     };
      var chart = new google.visualization.GeoChart(document.getElementById('visualization')); 
     chart.draw(data, options);
     }


  </script>
  <script>
    $(document).on('ready', function(){
      drawVisualization();

    });
    $(".enlarge").on('click', function(){
      drawVisualization();
    });
    $(window).on('resize', function(){
      drawVisualization();
    });
    
  </script>
  <script>
    $(document).ready(function(){
      $(window).on('resize load',  function(){
            var win = $(this); //this = window
            if (win.width() <= 550) {
              $(".sites-ref, .countries-ref").removeClass("col-xs-6").addClass("col-xs-12 center-block");
            }
            else if (win.width() >550) {
              $(".sites-ref, .countries-ref").removeClass("col-xs-12").addClass("col-xs-6");
            }
      });
    });
  </script>
  <script>
   $(document).ready(function(){
             $(".title-logo").css({width:'auto', height:'75px', top:'10px'}, 300, function(){});
             setTimeout(function() {
             $('.more').fadeIn(500, function(){});
             
         }, 1000);
   });
  </script>
  <script>
    $(document).ready(function(){
      var btnHits = $('.btn-hits').text();
      var totHits = $('.tot-hits').text();

      var ratio = Math.round(totHits/btnHits);
      

      $(".ratio-text").replaceWith("<p class='ratio-text'>1 button hit for every " + ratio + " visits to your URL</p>")
    });
  </script>
  <script type="text/javascript">
    $(".url-field").click(function(){
      $(this).select();
      document.execCommand("copy"); 
      alertify.success("Copied!");
    });
  </script>
  


  <?php echo $results["track"]; ?>
  </body>
</html>
