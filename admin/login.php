<?php
ob_start();

session_start();
ini_set('display_errors', 0);

if ($_SESSION["valid_user"]) {
    Header("Location: index.php");
}

include "../functions/database.php";
$query = $db->query("SELECT * FROM settings");

$result = $db->fetch_array($query);

$name = $result['name'];
$URL = $result['URL'];
$adminusr = $result['admin_user'];
$adminpss = $result['admin_pass'];



if ($_GET["op"] == "login") {
    if (!$_POST["username"] || !$_POST["password"]) {
        die("You need to provide a username and password.");
    }

    $user = $_POST['username'];
    $pass = $_POST['password'];
    $mdpass = md5($pass);



    if ($adminusr == $user AND $adminpss == $pass) {
        // Login good, create session variables
        $_SESSION["valid_id"] = $id;
        $_SESSION["valid_user"] = $_POST["username"];
        $_SESSION["valid_time"] = time();

        // Redirect to member page
        Header("Location: index.php");
    } else {
        // Login not successful
        echo "Username or Password is not correct!";
    }
} else {
    ?>

    <!DOCTYPE html>
    <html class="full" lang="en">
        <!-- The full page image background will only work if the html has the custom class set to it! Don't delete it! -->

        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="description" content="">
            <meta name="author" content="">

            <title>Sign In - <?php echo $name; ?></title>

            <!-- Bootstrap core CSS -->
            <link href="../css/bootstrap.min.css" rel="stylesheet">

            <!-- Custom CSS for the 'Full' Template -->
            <link href="../font/css/font-awesome.css" rel="stylesheet">
            <style>

            </style>
        </head>

        <body>

            <div class="container" style="margin-top:150px">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title"><strong>Sign in </strong></h3></div>
                        <div class="panel-body">
                            <form role="form" method="post" action="?op=login">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Username</label>
                                    <input type="text" class="form-control" style="border-radius:0px" name="username" placeholder="Enter Username">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password </label>
                                    <input type="password" class="form-control" style="border-radius:0px" name="password" placeholder="Password">
                                </div>
                                <button type="submit" class="btn btn-sm btn-default">Sign in</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- JavaScript -->
            <script src="js/jquery-1.10.2.js"></script>
            <script src="js/bootstrap.js"></script>
            <script src="valid/jquery.form-validator.js"></script>


        </body>

    </html>
    <?php
}
$db->close_connection();
?>
