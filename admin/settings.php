<?php
session_start();

if (!$_SESSION["valid_user"]) {
    Header("Location: login.php");
}

function validate_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

include "../functions/database.php";

$err = "nil";
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if ($_GET['op'] == "general") {

        $name = $db->escape_value(validate_input($_POST['web-name']));
        $URL = $db->escape_value(validate_input($_POST['url']));
        $logo = $db->escape_value(validate_input($_POST['logo']));
        $des = $db->escape_value(validate_input($_POST['des']));
        $cust = isset($_POST['cust']);

        // Check connection
        if (mysqli_connect_errno()) {
            $err = 1;
        };

        $querry = "UPDATE settings SET name='$name', URL='$URL', logo='$logo', des='$des', custom='$cust'";

        if ($db->query($querry)) {
            $err = 0;
        } else {
            $err = 1;
        };
    } elseif ($_GET['op'] == "download") {
        $prv = isset($_POST['priv']);
        $redirect = $db->escape_value($_POST['redirect']);

        // Check connection
        if (mysqli_connect_errno()) {
            $err = 1;
        };

        $querry = "UPDATE settings SET prv='$prv',redirect='$redirect' WHERE ID=1";

        if ($db->query($querry)) {
            $err = 0;
        } else {
            $err = 1;
        };
    } elseif ($_GET['op'] == "admin") {
        $admin_user = $db->escape_value(validate_input($_POST['adminuser']));
        $admin_pass = $db->escape_value(validate_input($_POST['adminpass']));
        $admin_passmd = md5($admin_pass);
        // Check connection
        if (mysqli_connect_errno()) {
            $err = 1;
        };

        $querry = "UPDATE settings SET admin_user='$admin_user', admin_pass='$admin_pass' WHERE ID=1";

        if ($db->query($querry)) {
            $err = 0;
        } else {
            $err = 1;
        };
    }
};

$query = $db->query("SELECT * FROM settings");
$result = $db->fetch_array($query);
$db->close_connection();
$admin_user = $result['admin_user'];
$admin_pass = $result['admin_pass'];
$name = $result['name'];
$URL = $result['URL'];
$logo = $result['logo'];
$des = $result['des'];
$prv = $result['prv'];
$cust = $result['custom'];
$redirect = $result['redirect'];
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo $name; ?> - Settings </title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.bootstrap.css" />
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-switch.min.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><?php echo $name; ?> Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li>
                        <a target="_blank" href="<?php echo $URL; ?>">View Site</a>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Admin <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="settings.php"><i class="fa fa-fw fa-gear"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li >
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li >
                            <a href="links.php"><i class="fa fa-fw fa-table"></i> Links</a>
                        </li> 
                        <li >
                            <a href="track.php"><i class="fa fa-fw fa-bar-chart-o"></i> Tracking</a>
                        </li>               
                        <li>
                            <a href="ads.php"><i class="fa fa-fw fa-money"></i> ADs</a>
                        </li>
                        <li  class="active">
                            <a href="settings.php"><i class="fa fa-fw fa-wrench"></i> Settings</a>
                        </li>


                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Settings
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-wrench"></i> Settings
                                </li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.row -->

                    <div class="row">

                        <div class="col-lg-8">
                            <div class="bs-example">
                                <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                                    <li class="active"><a href="#general" data-toggle="tab">General</a></li>
                                    <li><a href="#privacy" data-toggle="tab">Privacy</a></li>
                                    <li><a href="#admin" data-toggle="tab">Admin</a></li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div class="tab-pane fade active in" id="general">
                                        <form role="form" action="?op=general" method="post" >

                                            <div class="form-group">
                                                <label for="InputName">
                                                    Website Name
                                                </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-pencil "></i>
                                                    </span>
                                                    <input type="text" value="<?php echo $name; ?>"  name="web-name" class="form-control">
                                                </div>
                                            </div>  
                                            <div class="form-group">
                                                <label for="InputName">
                                                    URL <small> with no "/" at the end </small>
                                                </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa  fa-pencil "></i>
                                                    </span>
                                                    <input type="text" value="<?php echo $URL; ?>" name="url" class="form-control">
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                                <label for="InputName">
                                                    Description <small>Appears on search engines</small>
                                                </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa  fa-pencil "></i>
                                                    </span>
                                                    <input type="text" value="<?php echo $des; ?>" name="des"  class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="InputName">
                                                    Logo
                                                </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa  fa-pencil "></i>
                                                    </span>
                                                    <input type="text" value="<?php echo $logo; ?>" name="logo"  class="form-control">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="InputName">
                                                    Custom Alias 
                                                </label>
                                                <div class="input-group">
                                                    <?php
                                                    if ($cust) {
                                                        echo '<input type="checkbox" name="cust" checked data-on-color="success" data-off-color="danger" >';
                                                    } else {
                                                        echo '<input type="checkbox" name="cust" data-on-color="success" data-off-color="danger" >';
                                                    }
                                                    ?>
                                                </div>
                                            </div> 

                                            <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
                                        </form>
                                    </div>
                                    <div class="tab-pane fade " id="admin">
                                        <form role="form" action="?op=admin" method="post" >

                                            <div class="form-group">
                                                <label for="InputName">
                                                    Admin Username
                                                </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-pencil "></i>
                                                    </span>
                                                    <input type="text" value="<?php echo $admin_user; ?>"  name="adminuser" class="form-control" required>
                                                </div>
                                            </div>  
                                            <div class="form-group">
                                                <label for="InputName">
                                                    Password
                                                </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa  fa-pencil "></i>
                                                    </span>
                                                    <input type="password" value="<?php echo $admin_pass; ?>" name="adminpass" class="form-control" required>
                                                </div>
                                            </div> 


                                            <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
                                        </form>
                                    </div>

                                    <div class="tab-pane fade" id="privacy">
                                        <form role="form" action="?op=download" method="post" >

                                            <div class="form-group">
                                                <label for="InputName">
                                                    Private <small> admin only can access the website </small>
                                                </label>
                                                <div class="input-group">
                                                    <?php if ($prv == 0) {
                                                        ?>
                                                        <input type="checkbox" name="priv" data-on-color="success" data-off-color="danger" >
                                                    <?php
                                                    } elseif ($prv == 1) {
                                                        ?>
                                                        <input type="checkbox" name="priv" checked data-on-color="success" data-off-color="danger" >
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="InputName">
                                                    Redirection Link <small>Visitors will be redirected to this link when the website is private </small>
                                                </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa  fa-pencil "></i>
                                                    </span>
                                                    <input type="text" value="<?php echo $redirect; ?>" name="redirect"  class="form-control">
                                                </div>
                                            </div>
                                            <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">                </div>
                                        </form>
                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.row -->
                    <br>


                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/alertify.min.js"></script>
        <script src="js/bootstrap-switch.min.js"></script>
        <script>
            $('input:checkbox').bootstrapSwitch();
            var err = "";
            err = <?php echo '"' . $err . '"'; ?>;
            if (err === "1") {
                alertify.error("Failed to save settings");
            } else if (err === "0") {
                alertify.success("Settings Saved!");
            }
            ;
        </script>

    </body>

</html>
