<?php
session_start();

if (!$_SESSION["valid_user"]) {
    Header("Location: login.php");
}

include "../functions/database.php";

$err = "nil";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if ($_GET["op"] == "update") {
        $ad1 = $db->escape_value($_POST["ads1"]);
        $ad2 = $db->escape_value($_POST["ads2"]);
        $querry = "UPDATE settings SET ads1='$ad1', ads2='$ad2'";

        if (mysqli_connect_errno()) {
            $err = 1;
        };

        if ($db->query($querry)) {
            $err = 0;
        } else {
            $err = 1;
        };
    }
}

$query = $db->query("SELECT * FROM settings");

$result = $db->fetch_array($query);

$name = $result['name'];
$URL = $result['URL'];
$logo = $result['logo'];
$ads1 = $result['ads1'];
$ads2 = $result['ads2'];
$db->close_connection();
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo $name; ?> - Links </title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.bootstrap.css" />
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><?php echo $name; ?> Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li>
                        <a target="_blank" href="<?php echo $URL; ?>">View Site</a>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Admin <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="settings.php"><i class="fa fa-fw fa-gear"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li >
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li >
                            <a href="links.php"><i class="fa fa-fw fa-table"></i> Links</a>
                        </li>
                        <li >
                            <a href="track.php"><i class="fa fa-fw fa-bar-chart-o"></i> Tracking</a>
                        </li>
                        <li class="active">
                            <a href="ads.php"><i class="fa fa-fw fa-money"></i> ADs</a>
                        </li>
                        <li>
                            <a href="settings.php"><i class="fa fa-fw fa-wrench"></i> Settings</a>
                        </li>


                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Setup your Ads
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-money"></i> Ads
                                </li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <form role="form" action="?op=update" method="POST" >
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Enter your Ads Code (Entrie Website) <small>{HTML Format}</small> </label>
                                    <textarea name="ads1" class="form-control" rows="15"><?php echo $ads1; ?></textarea>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Enter your Ads Code (Shortened Link) <small>{HTML Format}</small> </label>
                                    <textarea name="ads2" class="form-control" rows="15"><?php echo $ads2; ?></textarea>
                                </div>
                            </div>
                            <button type="submit" name="submit" id="submit"  class="btn btn-primary">Save</button>
                        </form>
                    </div>
                    <!-- /.row -->
                    <br>


                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/alertify.min.js"></script>
        <script src="js/bootstrap-switch.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script>
            var err = "";
            err = <?php echo '"' . $err . '"'; ?>;
            if (err == "1") {
                alertify.error("Failed to save settings");
            } else if (err == "0") {
                alertify.success("Settings Saved!");
            }
            ;
        </script>
    </body>

</html>
