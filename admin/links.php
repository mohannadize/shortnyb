<?php
session_start();

if (!$_SESSION["valid_user"]) {
    Header("Location: login.php");
}
// include "../dbConfig.php";
include "../functions/database.php";
$query = $db->query("SELECT * FROM settings");

$result = $db->fetch_array($query);

$name = $result['name'];
$URL = $result['URL'];
$logo = $result['logo'];

$crud = $db->crud();
$db->close_connection();
//$query = $db->query("SELECT * FROM links WHERE DATE(date) = '' AND pos = 1");
//$result = $db->num_rows($query);
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo $name; ?> - Links </title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.bootstrap.css" />

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><?php echo $name; ?> Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li>
                        <a target="_blank" href="<?php echo $URL; ?>">View Site</a>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Admin <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="settings.php"><i class="fa fa-fw fa-gear"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li >
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li class="active">
                            <a href="links.php"><i class="fa fa-fw fa-table"></i> Links</a>
                        </li> 
                        <li>
                            <a href="track.php"><i class="fa fa-fw fa-bar-chart-o"></i> Tracking</a>
                        </li>               
                        <li>
                            <a href="ads.php"><i class="fa fa-fw fa-money"></i> ADs</a>
                        </li>
                        <li>
                            <a href="settings.php"><i class="fa fa-fw fa-wrench"></i> Settings</a>
                        </li>


                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Links
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-table"></i> links
                                </li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.row -->

                    <div class="row">

<?php

class crud {

    private $db;

    function __construct($DB_con) {
        $this->db = $DB_con;
    }

    /* paging */

    public function dataview($query) {
        $stmt = $this->db->prepare($query);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $count01 = mb_strlen($row['text']);
                if ($count01 > 30) {
                    $mytext = substr($row['text'], 0, 30) . "...";
                } else {
                    $mytext = $row['text'];
                }
                $count02 = mb_strlen($row['link']);
                if ($count02 > 30) {
                    $mylink = substr($row['link'], 0, 30) . "...";
                } else {
                    $mylink = $row['link'];
                }
                $count03 = mb_strlen($row['short']);
                if ($count03 > 10) {
                    $myshort = substr($row['short'], 0, 10) . "...";
                } else {
                    $myshort = $row['short'];
                }

                switch ($row['pos']) {
                    case 1:
                        $mypos = 'Tag';
                        break;
                    case 2:
                        $mypos = 'Top';
                        break;
                    case 3:
                        $mypos = 'Bottom';
                        break;
                }
                ?>
                                        <tr class="record">
                                        <!--<td><?php print($row['id']); ?></td> -->
                                            <td><a target="_blank" href="<?php echo '../' . $row['short']; ?>"><?php print($myshort); ?></a></td>
                                            <td title="<?php echo $row['link']; ?>"><a target="_blank" href="<?php echo $mylink; ?>"><?php print($mylink); ?></a></td>
                                            <td><?php print($mypos); ?></td>
                                            <td title="<?php echo $row['text']; ?>"><?php print($mytext); ?></td>
                                            <td><a target="_blank" href="<?php echo '../stats/' . $row['stats']; ?>"><?php print($row['stats']); ?></td>
                                            <td><?php print($row['date']); ?></td>
                                            <td align="center">
                                                <a target="popup"  onclick="window.open('links/edit.php?id=<?php print($row['id']); ?>', 'name', 'width=800,height=500')" ><i class="glyphicon glyphicon-edit"></i></a>
                                            </td>
                                            <td align="center">
                                                <a id="<?php echo $row['id']; ?>" class=" delbutton " title="Click To Delete" href="#"><i class="glyphicon glyphicon-remove-circle"></i></a>
                                            </td>
                                        </tr>
                <?php
            }
        } else {
            ?>
                                    <tr>
                                        <td>Nothing here...</td>
                                    </tr>
            <?php
        }
    }

    public function paging($query, $records_per_page) {
        $starting_position = 0;
        if (isset($_GET["page_no"])) {
            $starting_position = ($_GET["page_no"] - 1) * $records_per_page;
        }
        $query2 = $query . " limit $starting_position,$records_per_page";
        return $query2;
    }

    public function paginglink($query, $records_per_page) {

        $self = $_SERVER['PHP_SELF'];

        $stmt = $this->db->prepare($query);
        $stmt->execute();

        $total_no_of_records = $stmt->rowCount();

        if ($total_no_of_records > 0) {
            ?><ul class="pagination"><?php
                                    $total_no_of_pages = ceil($total_no_of_records / $records_per_page);
                                    $current_page = 1;
                                    if (isset($_GET["page_no"])) {
                                        $current_page = $_GET["page_no"];
                                    }
                                    if ($current_page != 1) {
                                        $previous = $current_page - 1;
                                        echo "<li><a href='" . $self . "?page_no=1'>First</a></li>";
                                        echo "<li><a href='" . $self . "?page_no=" . $previous . "'>Previous</a></li>";
                                    }
                                    for ($i = 1; $i <= $total_no_of_pages; $i++) {
                                        if ($i == $current_page) {
                                            echo "<li><a href='" . $self . "?page_no=" . $i . "' style='color:red;'>" . $i . "</a></li>";
                                        } else {
                                            echo "<li><a href='" . $self . "?page_no=" . $i . "'>" . $i . "</a></li>";
                                        }
                                    }
                                    if ($current_page != $total_no_of_pages) {
                                        $next = $current_page + 1;
                                        echo "<li><a href='" . $self . "?page_no=" . $next . "'>Next</a></li>";
                                        echo "<li><a href='" . $self . "?page_no=" . $total_no_of_pages . "'>Last</a></li>";
                                    }
                                    ?></ul><?php
                                }
                            }

                            /* paging */
                        }
                        ?>


                        <table class='table table-bordered table-responsive'>
                            <tr> 
                                <th>Short</th>
                                <th>Link</th>
                                <th>Position</th>
                                <th>Text</th>
                                <th>Stats</th>
                                <th>Date</th>
                                <th colspan="2" align="center">Actions</th>
                            </tr>
                        <?php
                        $query = "SELECT * FROM links";
                        $records_per_page = 10;
                        $newquery = $crud->paging($query, $records_per_page);
                        $crud->dataview($newquery);
                        ?>




                        </table>
                        <div class="pagination-wrap">
                        <?php $crud->paginglink($query, $records_per_page); ?>
                        </div>



                    </div>
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/alertify.min.js"></script>

        <script type="text/javascript">
                    $(function () {


                        $(".delbutton").click(function () {

                            //Save the link in a variable called element
                            var element = $(this);

                            //Find the id of the link that was clicked
                            var del_id = element.attr("id");

                            //Built a url to send
                            var info = 'ccz=' + del_id;
                            alertify.confirm("Are you SURE? You can't undo this step", function (e) {
                                if (e) {
                                    $.ajax({
                                        type: "GET",
                                        url: "links/delete.php",
                                        data: info,
                                        success: function () {

                                        }
                                    });
                                    element.parents(".record").animate({
                                        backgroundColor: "#fbc7c7"
                                    }, "fast")
                                            .animate({
                                                opacity: "hide"
                                            }, "slow");
                                    alertify.error("Link Deleted!");
                                } else {
                                    // user clicked "cancel"
                                    alertify.log("Link not deleted");
                                }
                            });


                            return false;

                        });

                    });

        </script>

    </body>

</html>
