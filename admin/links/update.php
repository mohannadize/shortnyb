<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    </head>
    <body>
        <link href="../../css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="../../font/css/font-awesome.min.css">
        <?php
        session_start();

        if (!$_SESSION["valid_user"]) {
            Header("Location: login.php");
        }
        include "../../functions/database.php";

        $id = $_POST["myid"];
        $link = $_POST["link"];
        $text = $_POST["ntext"];
        $pos = $_POST["pos"];
        $btnT = $_POST["btnT"];
        $btnL = $_POST["btnL"];
        $col = $_POST["col"];
        $icon = $_POST["icon"];



        $action = $db->query("UPDATE links SET link='$link', pos='$pos', text='$text', button='$btnT', btnL='$btnL', icon='$icon', color='$col'
WHERE id=$id ");
        if (!$action) {
            ?>
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <h2>Problem</h2>
                    <div class="bs-example">
                        <div class="alert alert-dismissable alert-danger">
                            <p>An error had occurred, <a href="edit.php?id=<? echo $id; ?>" class="alert-link">Please try again</a>.</p>
                        </div>
                    </div>
                </div>
            </div><!-- /.row -->
            <?php
        } else {
            ?>
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <h2>Updated Successfully</h2>
                    <div class="bs-example">
                        <div class="alert alert-dismissable alert-info">
                            <p> <a href="../links.php" class="alert-link">Close the tab to go back to links</a>.</p>
                        </div>
                    </div>
                </div>
            </div><!-- /.row -->

            <?php
        }
        $db->close_connection();
        ?>
        <script src="../../js/jquery-1.10.2.js"></script>
        <script src="../../js/bootstrap.js"></script>
        <script type="text/javascript">
            function CloseMe()
            {
                window.close();
            }
            ;
            window.setTimeout(CloseMe, 2000);
        </script>
    </body>
</html>