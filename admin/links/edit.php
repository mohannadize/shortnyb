<?php
session_start();

if (!$_SESSION["valid_user"]) {
    Header("Location: login.php");
}
include "../../functions/database.php";
$id = $_GET['id'];
$query = $db->query("SELECT * FROM links where id='$id' ");

$result = $db->fetch_array($query);

$shr = $result['short'];
$link = $result['link'];
$pos = $result['pos'];
$text = $result['text'];
$button = $result['button'];
$btnL = $result['btnL'];
$icon = $result['icon'];
$color = $result['color'];

$db->close_connection();

if ($shr == '') {
    echo "Link not found, it maybe deleted!";
} else {
    ?>

    <head>
        <link href="../../css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="../../font/css/font-awesome.min.css">

    </head>
    <body>
        <div class="container" style="padding-bottom: 50px;padding-top: 25px;">
            <div class="row">

                <form role="form" action="update.php" method="post" >
                    <div class="col-lg-6">
                        <div id="legend">
                            <legend class="">
                                Edit Link
                            </legend>
                        </div>
                        <div class="form-group">

                            <input type="hidden" value="<?php echo $id; ?>" class="form-control" name="myid" >

                        </div>  
                        <div class="form-group">
                            <label for="InputName">
                                Short
                            </label>
                            <div class="input-group">
                                <input type="text" value="<? echo $shr; ?>" disabled title="You can't change this for security reason" class="form-control">
                                <input type="hidden" value="<? echo $shr; ?>"  name="myshr" class="form-control">
                                <span class="input-group-addon">
                                    <i class="fa fa-check-square-o form-control-feedback">
                                    </i>
                                </span>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label for="type">
                                Link
                            </label>
                            <div class="input-group">
                                <input type="text" value="<? echo $link; ?>" name="link" class="form-control">
                                <span class="input-group-addon">
                                    <i class="fa fa-pencil form-control-feedback">
                                    </i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nname">
                                Text
                            </label>
                            <div class="input-group">
                                <input type="text" value="<? echo $text; ?>" class="form-control" name="ntext" >
                                <span class="input-group-addon">
                                    <i class="fa fa-pencil form-control-feedback">
                                    </i>
                                </span>
                            </div>
                        </div>     
                        <div class="form-group">
                            <label for="nname">
                                Position
                                <small>:: 1 for Tag, 2 for Top banner, 3 for Bottom banner </small>
                            </label>
                            <div class="input-group">
                                <input type="text" value="<? echo $pos; ?>" id="pos" class="form-control" name="pos" >
                                <span class="input-group-addon">
                                    <i class="fa fa-pencil form-control-feedback">
                                    </i>
                                </span> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nname">
                                Button Text
                                <small>:: Leave blank for no button</small>
                            </label>
                            <div class="input-group">
                                <input type="text" value="<? echo $button; ?>" class="form-control" name="btnT" >
                                <span class="input-group-addon">
                                    <i class="fa fa-pencil form-control-feedback">
                                    </i>
                                </span>
                            </div>
                        </div>  
                        <div class="form-group">
                            <label for="nname">
                                Button Link
                                <small>:: Leave it blank to redirect to long URL</small>
                            </label>
                            <div class="input-group">
                                <input type="text" value="<? echo $btnL; ?>"  class="form-control" name="btnL" >
                                <span class="input-group-addon">
                                    <i class="fa fa-pencil form-control-feedback">
                                    </i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nname">
                                Color
                                <small>:: 1 - Pink, 2 - Red, 3 - Yellow , 4 - Green , 5 - Blue, 6 - Purple </small>
                            </label>
                            <div class="input-group">
                                <input type="text" value="<? echo $color; ?>" class="form-control" name="col" >
                                <span class="input-group-addon">
                                    <i class="fa fa-pencil form-control-feedback">
                                    </i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nname">
                                Icon
                                <small>:: 1 - Pink, 2 - Red, 3 - Yellow , 4 - Green , 5 - Blue, 6 - Purple </small>
                            </label>
                            <div class="input-group">
                                <input type="text" value="<? echo $icon; ?>" class="form-control" name="icon" >
                                <span class="input-group-addon">
                                    <i class="fa fa-pencil form-control-feedback">
                                    </i>
                                </span>
                            </div>
                        </div> 
                        <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
                    </div>
                </form>
                <hr class="featurette-divider hidden-lg">

            </div>

        </div>

        <script src="../../js/jquery-1.10.2.js"></script>
        <script src="../../js/bootstrap.js"></script>
        <script src="../../valid/jquery.form-validator.js"></script>



        <script>
            $.validate({
                modules: 'security'
            });
        </script>


    </body>
    <?php
}
?>