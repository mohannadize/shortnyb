<?php
include "functions/database.php";
$query = $db->query("SELECT * FROM settings");

$result = $db->fetch_array($query);
$prv = $result['prv'];
if ($prv) {
    session_start();
    if (empty($result['redirect']) && empty($_SESSION["valid_user"])) {
        $db->close_connection();
        Header("Location: admin/login.php");
        exit;
    } elseif (empty($_SESSION["valid_user"])) {
        $db->close_connection();
        Header("Location: " . $result["redirect"]);
        exit;
    }
}
$name = $result['name'];
$URL = $result['URL'];
$logo = $result['logo'];
$des = $result['des'];
$ads1 = $result['ads1'];
include "functions/random.php";


$surl = $db->escape_value($_POST['nlink']);
$spos = $db->escape_value($_POST['pos']);
$scol = preg_replace("/[^a-z0-9.]+/i", '', $db->escape_value($_POST['col']));
$stext = $db->escape_value($_POST['text']);
$sbtnT = $db->escape_value($_POST['btnT']);
$sicon = $db->escape_value($_POST['icon']);
$sbtnL = $db->escape_value($_POST['btnL']);
$scust = $db->escape_value($_POST['short']);
$sstats = $rand3 . '' . $rand4;
if (!preg_match("/^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i", $surl)) {
    exit;
};
$linko = preg_replace("/(?:https?:\/\/)?(?:(?:(?:www\.?)?youtube\.com(?:\/(?:(?:watch\?.*?(v=[^&\s]+).*)|(?:v(\/.*))))?)|(?:youtu\.be(\/.*)?))/i", "http://www.youtube.com/embed/$1$2$3", $surl);
$erroriframe = 0;
if (stripos($linko, "messenger.com")) {
    $erroriframe = 1;
};
$urlhere = $linko;
$headers = get_headers($urlhere);
$headers = implode("", $headers);
if (stripos($headers, "X-Frame-Options: DENY") > -1 || stripos($headers, "X-Frame-Options: SAMEORIGIN")) {
    $erroriframe = 1;
};
if ($scust == '') {
    $query = $db->query("SELECT * FROM links WHERE BINARY short='$rand'");

    if (mysqli_num_rows($query) > 0) {
        $scust = $rand2;
    } else {
        $scust = $rand;
    }
    $db->query("INSERT INTO links (link, short,iframe, pos, text, button, btnL, icon, color, stats) VALUES ('$surl', '$scust','$erroriframe','$spos','$stext','$sbtnT','$sbtnL','$sicon','$scol','$sstats')");
} else {
    $lc = strtolower($scust);
    switch ($lc) {
        case 'admin':
            exit;
            break;
        case 'js':
            exit;
            break;
        case 'font':
            exit;
            break;
        case 'functions':
            exit;
            break;
        case 'img':
            exit;
            break;
        case 'install':
            exit;
            break;
        case 'css':
            exit;
            break;
    }
    $test = $db->query("SELECT * FROM links WHERE BINARY short='$scust'");
    if ($db->num_rows($test)) {
        exit;
    } else {
        $db->query("INSERT INTO links (link, short,iframe, pos, text, button, btnL, icon, color, stats) VALUES ('$surl', '$scust','$erroriframe','$spos','$stext','$sbtnT','$sbtnL','$sicon','$scol','$sstats')");
    }
}
switch ($spos) {
    case '1':
        $db->query("UPDATE stats SET tag=tag+1");
        break;
    case '2':
        $db->query("UPDATE stats SET top=top+1");
        break;
    case '3':
        $db->query("UPDATE stats SET bottom=bottom+1");
        break;
}
switch ($scol) {
    case '1':
        $db->query("UPDATE stats SET pink=pink+1");
        break;
    case '2':
        $db->query("UPDATE stats SET red=red+1");
        break;
    case '3':
        $db->query("UPDATE stats SET yellow=yellow+1");
        break;
    case '4':
        $db->query("UPDATE stats SET green=green+1");
        break;
    case '5':
        $db->query("UPDATE stats SET blue=blue+1");
        break;
    case '6':
        $db->query("UPDATE stats SET purple=purple+1");
        break;
}
$db->close_connection();
?>
<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">


        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <title><?php echo $name; ?> - Link Created</title>

        <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />

        <!--- Font-Awesome CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!--- Core CSS-->
        <link href="css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/normalize.css" media="all" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.bootstrap.css" />

        <!-- Custom CSS -->
        <link href="css/style.css" media="all" rel="stylesheet" type="text/css" />

        <!-- Animate.css -->
        <link href="css/animate.css" rel="stylesheet" type="text/css"/>

        <!--- selectBoxIt CSS -->
        <link type="text/css" rel="stylesheet" href="//gregfranko.com/jquery.selectBoxIt.js/css/jquery.selectBoxIt.css" />

        <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
        <script src="http://gregfranko.com/jquery.selectBoxIt.js/js/jquery.selectBoxIt.min.js"></script>
        <script src="js/stylefill.js" type="text/javascript"></script>
        <script src="js/bigtext.js" type="text/javascript"></script>    


    </head>

    <body class="home-bg">




        <div class="container">


            <a href="<?php echo $URL; ?>"><img alt="Logo" class="title-logo center-block small" src="<?php echo $logo; ?>"/></a>


            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <div class="input col-xs-10 col-xs-offset-1">
                        <p class="input-label">Your shortened URL</p>
                        <input type="text" readonly name="shortURL" class="url-field form-control url-field anim" autocomplete="off" id="short" value="<?php echo $URL . '/' . $scust; ?>"></input>
                    </div>

                    <div class="more">
                        <div class="col-xs-4 col-xs-offset-1 stat-share">
                            <a href="stats/<?php echo $sstats; ?>" class="btn btn-default stats-btn col-xs-12 anim-fast mrgn-top"><i class="ion-stats-bars"></i> Stats</a>

                            <p class="input-label text-center">Share</p>
                            <div class="col-xs-6">
                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $URL . '/' . $scust; ?>" target="_blank"> <button class="btn social-btn anim pull-right mrgn-top fb"><i class="ion-social-facebook"></i></button></a>
                            </div>
                            <div class="col-xs-6">
                                <a href="https://twitter.com/home?status=<?php echo $URL . '/' . $scust; ?>" target="_blank"><button class="btn social-btn anim pull-left mrgn-top twit"><i class="ion-social-twitter"></i></button></a>
                            </div>
                            <div class="col-xs-6">
                                <a href="https://pinterest.com/pin/create/button/?url=<?php echo $URL . '/' . $scust; ?>" target="_blank"><button class="btn social-btn anim pull-right mrgn-top pinterest"><i class="ion-social-pinterest"></i></button></a>
                            </div>
                            <div class="col-xs-6">
                                <a href="https://plus.google.com/share?url=<?php echo $URL . '/' . $scust; ?>" target="_blank">  <button class="btn social-btn anim pull-left mrgn-top gplus"><i class="ion-social-googleplus"></i></button></a>
                            </div>

                        </div>
                        <div class="col-xs-5 col-xs-offset-1 mrgn-top qr">
                            <p class="input-label text-center">QR Code</p>
                           <!-- <img alt="QRCODE" src="img/qr.png" class="qrcode center-block"/>  -->
                            <img alt="QRCODE" src="https://chart.googleapis.com/chart?chs=290x290&cht=qr&chl=<?php echo $URL . '/' . $scust; ?>" class="qrcode center-block"/>
                        </div>

                    </div> <!-- END .more -->






                </div>
            </div>


        </div>

        <div style="display:block;padding-top:30px;padding-bottom:30px;margin:auto;"><?php echo $ads1; ?></div>



        <script src="js/alertify.min.js"></script>




        <script>
            $(document).ready(function () {
                $(window).on('resize load', function () {
                    var win = $(this); //this = window
                    if (win.width() <= 465) {
                        $(".stat-share").removeClass("col-xs-4 col-xs-offset-1").addClass("col-xs-10 col-xs-offset-1");
                        $(".qr").removeClass("col-xs-5 col-xs-offset-1").addClass("col-xs-10 col-xs-offset-1");
                    } else if (win.width() > 465) {
                        $(".stat-share").removeClass("col-xs-10 col-xs-offset-1").addClass("col-xs-4 col-xs-offset-1");
                        $(".qr").removeClass("col-xs-10 col-xs-offset-1").addClass("col-xs-5 col-xs-offset-1");

                    }
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    $(".title").css({fontSize: '50px', top: '10px'});
                    $('.more').addClass("animated fadeInUp");
                    $(".more").show();
                }, 700);
            });
        </script>
        <script>
            $(".url-field").click(function () {
                $(this).select();
                document.execCommand("copy");
                alertify.success("Copied!");
                // $(this).blur();
            });
        </script>


    </body>
</html>
