<?php

ini_set('display_errors', 0);
ini_set('log_errors', 0);
if (!empty($_POST["chklinkjs"])) {
    $lc = strtolower($_POST['chklinkjs']);
    switch ($lc) {
        case 'admin':
            echo 1;
            exit;
            break;
        case 'js':
            echo 1;
            exit;
            break;
        case 'font':
            echo 1;
            exit;
            break;
        case 'functions':
            echo 1;
            exit;
            break;
        case 'img':
            echo 1;
            exit;
            break;
        case 'install':
            echo 1;
            exit;
            break;
        case 'css':
            echo 1;
            exit;
            break;
    }
    include "functions/database.php";
    $checklink = $db->escape_value($_POST['chklinkjs']);
    $result = $db->query("SELECT * FROM links WHERE BINARY short='$checklink'");
    if ($db->num_rows($result)) {
        echo 1;
        $db->close_connection();
        exit;
    } else {
        echo 0;
        $db->close_connection();
        exit;
    };
} elseif (!empty($_POST['iframe'])) {
    $error = 0;
    if (stripos($_POST['iframe'], "messenger.com")) {
        echo 1;
        exit;
    };
    if (stripos($_POST['iframe'], "youtu.be")) {
        echo 0;
        exit;
    };

    $urlhere = $_POST['iframe'];
    $urlhere = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "http://www.youtube.com/embed/$1", $urlhere);
    $headers = get_headers($urlhere);
    $headers = implode("", $headers);
    if (stripos($headers, "X-Frame-Options: DENY") > -1 || stripos($headers, "X-Frame-Options: SAMEORIGIN")) {
        $error = 1;
    };
    echo $error;
    exit;
}
$db->close_connection();
?>