<?php
include "functions/database.php";

$sql = 'SELECT name, URL, logo, des FROM settings'; //specify columns explicitly 
$results = $db->query($sql);
$results = $db->fetch_array($results);
$name = $results['name'];
$URL = $results["URL"];
$des = $results['des'];
$db->close_connection();
?>
<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">


        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <title><?php echo $name; ?></title>
        <meta name="description" content="<?php echo $des; ?>">

        <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />

        <!--- Font-Awesome CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!--- Core CSS-->
        <link href="css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/normalize.css" media="all" rel="stylesheet" type="text/css" />

        <!-- Custom CSS -->
        <link href="css/style.css" media="all" rel="stylesheet" type="text/css" />

        <!-- Animate.css -->
        <link href="css/animate.css" rel="stylesheet" type="text/css"/>

        <!--- selectBoxIt CSS -->
        <link type="text/css" rel="stylesheet" href="//gregfranko.com/jquery.selectBoxIt.js/css/jquery.selectBoxIt.css" />

        <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
        <script src="http://gregfranko.com/jquery.selectBoxIt.js/js/jquery.selectBoxIt.min.js"></script>
        <script src="js/stylefill.js" type="text/javascript"></script>
        <script src="js/bigtext.js" type="text/javascript"></script>
        <script src="js/jquery.fittext.js" type="text/javascript"></script>
        <script src="js/flowtype.js" type="text/javascript"></script>  
        <script src="js/jquery.debouncedresize.js" type="text/javascript"></script>  
        <script src="js/jquery.gsap.js" type="text/javascript"></script>
        <script src="js/TweenMax.js" type="text/javascript"></script>
        <script src="js/EasePack.js" type="text/javascript"></script>


    </head>

    <body class="home-bg">



        <div class="container">
            <div class="row">

                <div class="major col-xs-12">

                    <a class="not-link btn home-btn anim-fast" href="<?php echo $URL; ?>"><i class="fa fa-home"></i></a>

                    <div class="col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2 error-container">
                        <img alt="outline..." class="outline" src="img/outlineb.png"/>
                        <div class="error-body">
                            <div class="error1">
                                <h1>404</h1>
                                <br>
                                <span>Apparently, the page you're looking for can't be found...</span>
                            </div>
                            <br>
                            <!---<h3>Apparently, the page you're looking for can't be found..</h3>-->
                        </div>
                    </div>


                </div>


        <!---<img class="error-img col-sm-2 col-sm-offset-5 col-xs-12 animated fadeIn" alt="Error404" src="img/404.png"/>-->

            </div>
        </div>

        <!--- pink red yellow green blue purple -->

        <script>
            var winHeight = parseInt($(window).innerHeight(), 10);
            var winWidth = parseInt($(window).innerWidth(), 10);
            var errHeight = parseInt($(".error-container").innerHeight(), 10);

            $(document).ready(function () {
                $(".major").css({'height': 10 + 'px', 'width': '10px', 'top': winHeight / 2 - 10 + 'px', 'left': winWidth / 2 - 10 + 'px', 'border-radius': '100%'});
                $(".error-container").css({'top': winHeight / 2 - errHeight / 2 + 'px'});
                setTimeout(function () {
                    $(".major").animate({'height': winHeight - 20 + 'px', 'width': winWidth - 20 + 'px', 'top': '10px', 'left': '10px', 'border-radius': '10px'}, 500, "easeInOutQuad", function () {});
                    setTimeout(function () {
                        $(".error-container, .home-btn").animate({'opacity': '1'}, 1000, "easeOutQuint", function () {});
                    }, 700);
                }, 400);



                $(window).on('debouncedresize', function () {
                    var winHeight = parseInt($(window).innerHeight(), 10);
                    var winWidth = parseInt($(window).innerWidth(), 10);

                    $(".major").animate({'height': winHeight - 20 + 'px', 'width': winWidth - 20 + 'px', 'top': '10px', 'left': '10px', 'border-radius': '10px'}, 500, "easeInOutQuad", function () {});
                    $(".error-container").css({'top': winHeight / 2 - errHeight / 2 + 'px'});
                    /*$('.error-body h1').fitText(0.8, {
                     minFontSize: '30px',
                     maxFontSize: '70px'
                     });*/
                    /*$(".error-body h1").flowtype({
                     minFont:30,
                     maxFont:70
                     });
                
                     $(".error-body span").flowtype({
                     minFont:13,
                     maxFont: 30
                     });*/
                });

            });


        </script>


    </body>
</html>
