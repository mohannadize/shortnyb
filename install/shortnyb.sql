-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2016 at 06:51 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` text NOT NULL,
  `short` varchar(50) NOT NULL,
  `iframe` int(1) NOT NULL,
  `pos` int(1) NOT NULL DEFAULT '1',
  `text` text NOT NULL,
  `button` text NOT NULL,
  `btnL` text NOT NULL,
  `icon` int(1) NOT NULL,
  `color` int(1) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stats` varchar(100) NOT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `statsdata` longtext NOT NULL,
  `refererdata` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `links`
--

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` double NOT NULL,
  `name` varchar(64) NOT NULL,
  `URL` varchar(255) NOT NULL,
  `custom` int(1) NOT NULL DEFAULT '1',
  `admin_user` varchar(255) NOT NULL,
  `admin_pass` varchar(255) NOT NULL,
  `prv` int(1) NOT NULL DEFAULT '0',
  `redirect` text NOT NULL,
  `logo` varchar(255) NOT NULL DEFAULT 'img/logo.png',
  `des` text NOT NULL,
  `track` text NOT NULL,
  `ads1` text NOT NULL,
  `ads2` text NOT NULL,
  PRIMARY KEY (`admin_user`),
  UNIQUE KEY `URL` (`URL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `URL`, `custom`, `admin_user`, `admin_pass`, `prv`, `redirect`, `logo`, `des`, `track`, `ads1`, `ads2`) VALUES
(1, 'ShortnyB', 'DO NOT FORGET THIS', 1, 'admin', 'admin', 1, '', 'img/logo-dark.png', '', ' ', ' ', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `stats`
--

CREATE TABLE IF NOT EXISTS `stats` (
  `id` double NOT NULL,
  `pink` int(11) NOT NULL DEFAULT '0',
  `red` int(11) NOT NULL DEFAULT '0',
  `yellow` int(11) NOT NULL DEFAULT '0',
  `green` int(11) NOT NULL DEFAULT '0',
  `blue` int(11) NOT NULL DEFAULT '0',
  `purple` int(11) NOT NULL DEFAULT '0',
  `top` int(11) NOT NULL DEFAULT '0',
  `tag` int(11) NOT NULL DEFAULT '0',
  `bottom` int(11) NOT NULL DEFAULT '0',
  `todayhits` int(11) NOT NULL DEFAULT '0',
  `totalhits` bigint(20) NOT NULL DEFAULT '0',
  `reset` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stats`
--

INSERT INTO `stats` (`id`, `pink`, `red`, `yellow`, `green`, `blue`, `purple`, `top`, `tag`, `bottom`, `todayhits`, `totalhits`, `reset`) VALUES
(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
