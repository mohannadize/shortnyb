<?php

ini_set('display_errors', 0);
ini_set('log_errors', 0);

class MySQLDatabase {

    private $host = "poop1"; //the host
    private $db_user = "poop2";  //Your username
    private $pass = "poop3";  // Your password
    private $dbase = "poop4"; // database name
    private $connection;

    function __construct() {
        $this->open_connection();
    }

    public function crud() {
        try {
            $DB_con = new PDO("mysql:host={$this->host};dbname={$this->dbase}", $this->db_user, $this->pass);
            $DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo $e->getMessage();
        };
        return new crud($DB_con);
    }

    public function open_connection() {
        $this->connection = mysqli_connect($this->host, $this->db_user, $this->pass, $this->dbase);
        if (mysqli_connect_errno()) {
            die("Database connection failed: " .
                    mysqli_connect_error() .
                    " (" . mysqli_connect_errno() . ")"
            );
        }
    }

    public function close_connection() {
        if (isset($this->connection)) {
            mysqli_close($this->connection);
            unset($this->connection);
        }
    }

    public function query($sql) {
        $result = mysqli_query($this->connection, $sql);
        $this->confirm_query($result);
        return $result;
    }

    private function confirm_query($result) {
        if (!$result) {
            die(mysqli_error($this->connection));
        }
    }

    public function escape_value($string) {
        $escaped_string = mysqli_real_escape_string($this->connection, $string);
        return $escaped_string;
    }

    // "database neutral" functions

    public function fetch_array($result_set) {
        return mysqli_fetch_assoc($result_set);
    }

    public function num_rows($result_set) {
        return mysqli_num_rows($result_set);
    }

    public function insert_id() {
        // get the last id inserted over the current db connection
        return mysqli_insert_id($this->connection);
    }

    public function affected_rows() {
        return mysqli_affected_rows($this->connection);
    }

    public function increstats($country, $shr) {
        $con = $this->connection;
        $result = mysqli_query($con, "SELECT statsdata,refererdata,hits FROM links WHERE BINARY short='$shr'");
        $result2 = mysqli_query($con, "SELECT todayhits,reset FROM stats");
        $result2 = mysqli_fetch_assoc($result2);
        $result2 = $result2["reset"];
        $result = mysqli_fetch_assoc($result);
        $referers = $result["refererdata"];
        $stats = $result["statsdata"];
        $hits = $result["hits"];
        $hits++;
        if ($stats == "") {
            $stats = [[], []];
            $stats[0][0] = 1;
            $stats[1][0] = $country;
        } else {
            $stats = explode("&", $stats);
            $stats[0] = explode("_", $stats[0]);
            $stats[1] = explode("_", $stats[1]);


            if (in_array($country, $stats[1])) {
                $pos = array_search($country, $stats[1]);
                $stats[0][$pos] ++;
            } else {
                $stats[0][count($stats[0])] = 1;
                $stats[1][count($stats[1])] = $country;
            }
        }
        $stats[0] = implode("_", $stats[0]);
        $stats[1] = implode("_", $stats[1]);
        $stats = implode("&", $stats);


        if (!empty($_SERVER["HTTP_REFERER"])) {
            $referer = parse_url($_SERVER["HTTP_REFERER"]);
            $referer = $referer["host"];
            if ($referers == "") {
                $referers = [[], []];
                $referers[0][0] = 1;
                $referers[1][0] = $referer;
            } else {
                $referers = explode("&", $referers);
                $referers[0] = explode("_", $referers[0]);
                $referers[1] = explode("_", $referers[1]);
                if (in_array($referer, $referers[1])) {
                    $pos = array_search($referer, $referers[1]);
                    $referers[0][$pos] ++;
                } else {
                    $referers[0][count($referers[0])] = 1;
                    $referers[1][count($referers[1])] = $referer;
                }
            };
            $referers[0] = implode("_", $referers[0]);
            $referers[1] = implode("_", $referers[1]);
            $referers = implode("&", $referers);
        };
        mysqli_query($con, "UPDATE links SET statsdata='$stats', refererdata='$referers', hits='$hits' WHERE BINARY short='$shr'");
        if (date("Y-m-d") == $result2) {
            mysqli_query($con, "UPDATE stats SET totalhits=totalhits+1, todayhits=todayhits+1");
        } else {
            $today = date("Y-m-d");
            mysqli_query($con, "UPDATE stats SET totalhits=totalhits+1, todayhits=1,reset='$today'");
        }
    }

    public function increbuttonclick($shr) {
        $con = $this->connection;
        mysqli_query($con, "UPDATE links SET clicks = clicks + 1 WHERE BINARY short='$shr'");
    }

}

$database = new MySQLDatabase();
$db = & $database;
?>