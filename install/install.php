<?php
ini_set('display_errors', 0);
ini_set('log_errors', 0);
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!empty($_POST["req"])) {
        if (($_POST["req"] == "poop") && (!empty($_POST["dbhost"])) && (!empty($_POST["dbuser"])) && (!empty($_POST["dbname"]))) {
            $dbhost = $_POST['dbhost'];
            $dbuser = $_POST["dbuser"];
            $dbpass = $_POST["dbpass"];
            $dbname = $_POST["dbname"];
            $con = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
            if ($con) {
                mysqli_close($con);
                echo 1;
                exit;
            } else {
                echo 0;
                exit;
            };
        } else {
            echo 0;
            exit;
        };
    } else {
        $dbhost = $_POST['dbhost'];
        $dbuser = $_POST["dbuser"];
        $dbpass = $_POST["dbpass"];
        $dbname = $_POST["dbname"];
        $check = [$dbhost, $dbname, $dbuser];
        foreach ($check as $value) {
            if ($value == "") {
                exit;
            }
        };
        $change = file_get_contents("database.php");
        $change = str_replace("poop1", $dbhost, $change);
        $change = str_replace("poop2", $dbuser, $change);
        $change = str_replace("poop3", $dbpass, $change);
        $change = str_replace("poop4", $dbname, $change);
        file_put_contents("../functions/database.php", $change);
        $change2 = file_get_contents("../.htaccess");
        include "../functions/database.php";
        $filename = 'shortnyb.sql';
        $templine = '';
        $lines = file($filename);
        foreach ($lines as $line) {
            if (substr($line, 0, 2) == '--' || $line == '')
                continue;
            $templine .= $line;
            if (substr(trim($line), -1, 1) == ';') {
                $db->query($templine);
                $templine = '';
            }
        }
    }
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <title>ShortnyB - Install</title>
            <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
            <link href="../css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
            <link href="../css/normalize.css" media="all" rel="stylesheet" type="text/css" />
            <script src="../js/jquery-1.10.2.js" type="text/javascript"></script>
            <script src="../js/bootstrap.js" type="text/javascript"></script>
            <style type="text/css">
                body {
                    background-color: #0A0A0A;
                    font-family: Roboto;
                }
                .container {
                    width: 60vw;
                    margin-top: 100px;
                }
                #dbinfo button {
                    margin-left: -20px;
                }
                .container h3 {
                    margin-bottom: 10px;
                }
                .container img {
                    display: block;
                    margin: auto;
                    width: 10vw;
                    position: absolute;
                    bottom: 0;
                    right: 0;
                }
                .shadow {
                    box-shadow: 0 3px 7px -2px rgba(0,0,0,1);
                }
            </style>
        </head>
        <body>
            <div class="container">
                <img src="cz.png">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="">
                            <div class="alert alert-dismissable alert-success">
                                <h3>Successfully installed!</h3>
                                <p>You can now delete the install folder!</p>
                                <p>Default admin username and pass is "admin".</p>
                                <p>Check your admin panel and make sure you change the URL to your website URL.</p>
                                <p>#Do not forget to read the documentation <br>

                                    #Do not hesitate to contact our 5-Star Support :)<br>

                                    #Thanks for Choosing Condize :) <3<br></p>
                                <p><a href="../admin/settings.php">Click Here to get to your admin panel!</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>
    </html>
<?php }; ?>