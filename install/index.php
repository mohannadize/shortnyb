<!DOCTYPE html>
<html>
    <head>
        <title>ShortnyB - Install</title>
        <!-- <base href="install/"> -->
        <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href="../css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
        <link href="../css/normalize.css" media="all" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
        <script src="../js/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="../js/bootstrap.js" type="text/javascript"></script>
        <style type="text/css">
            body {
                background-color: #0A0A0A;
                font-family: Roboto;
            }
            .container {
                width: 60vw;
                margin-top: 100px;
            }
            #dbinfo button {
                /*margin-left: -20px;*/
            }
            .container h4 {
                margin-bottom: 15px;
                color: white;
                font-weight: 300;
                text-shadow: 2px 1px 3px rgba(0,0,0,1);
            }
            .container img {
                display: block;
                margin: auto;
                width: 10vw;
                position: absolute;
                bottom: 0;
                right: 0;
            }
            .shadow {
                box-shadow: 0 3px 7px -2px rgba(0,0,0,1);
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div>
                <img src="cz.png">
                <h4>Create a new database and Username then enter your credentials here! :D</h4>
                <h4>Please enter your database information:</h4>
                <form id="dbinfo" autocomplete="off" action="javascript:chkFoorm()">
                    <div class="row">
                        <div class="form-group col-sm-10 col-md-6 col-lg-4">
                            <input type="text" class="form-control shadow" poop-name="dbhost" placeholder="Database Host" required>
                        </div>
                        <br>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-10 col-md-6 col-lg-4">
                            <input type="text" class="form-control shadow" poop-name="dbuser" placeholder="Username" required>
                        </div>
                        <br>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-10 col-md-6 col-lg-4">
                            <input type="password" class="form-control shadow" poop-name="dbpass" placeholder="Password">
                        </div>
                        <br>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-10 col-md-6 col-lg-4">
                            <input type="text" class="form-control shadow" poop-name="dbname" placeholder="Database Name" required>
                        </div>
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-xs-offset-2">
                            <button type="submit" class="btn btn-primary shadow">Install</button>
                        </div>
                    </div>
                    <p style="text-shadow: 2px 1px 3px rgba(0,0,0,1);color:white; margin-top:10px;font-weight:300;font-size:16px;">If nothing happened after you click the button make sure Javascript is enabled and double check your info.</p style="color:white; margin-top:10px;font-weight:300;font-size:16px;">
                </form>
            </div>
        </div>
        <script type="text/javascript">
            function chkFoorm() {
                var array1 = {req: "poop"};
                $("#dbinfo").find("input").each(function () {
                    array1[$(this).attr("poop-name")] = $(this).val();
                });
                $.post("install.php", array1, function (value) {
                    if (value == 1) {
                        $("#dbinfo").find("input").each(function () {
                            var holding = $(this).attr("poop-name");
                            $(this).attr("name", holding);
                        });
                        $("#dbinfo").attr("action", "install.php");
                        $("#dbinfo").attr("method", "POST");
                        $("#dbinfo button[type=submit]").click();
                    } else {
                        alert("One or more input is wrong!");
                    }
                    ;
                });
            }
        </script>
    </body>
</html>