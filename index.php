<?php
include "functions/database.php";
$sql = 'SELECT name,URL,logo,des,prv,redirect,track,ads1 FROM settings'; //specify columns explicitly 
$results = $db->query($sql);
$results = $db->fetch_array($results);
$prv = $results['prv'];
if ($prv) {
    session_start();
    if (empty($results['redirect']) && empty($_SESSION["valid_user"])) {
        $db->close_connection();
        Header("Location: admin/login.php");
        exit;
    } elseif (empty($_SESSION["valid_user"])) {
        $db->close_connection();
        Header("Location: " . $results["redirect"]);
        exit;
    }
}
$name = $results['name'];
$URL = $results["URL"];
$logo = $results["logo"];
$des = $results['des'];
$ads1 = $results['ads1'];
$db->close_connection();
?>
<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">


        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <title><?php echo $name; ?></title>
        <meta name="description" content="<?php echo $des; ?>">

        <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />

        <!--- Font-Awesome CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!--- Core CSS-->
        <link href="css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/normalize.css" media="all" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/alertify.core.css" />
        <link rel="stylesheet" href="css/alertify.default.css" />
        <!-- Custom CSS -->
        <link href="css/style.css" media="all" rel="stylesheet" type="text/css" />

        <!-- Animate.css -->
        <link href="css/animate.css" rel="stylesheet" type="text/css"/>

        <!--- selectBoxIt CSS -->
        <link type="text/css" rel="stylesheet" href="//gregfranko.com/jquery.selectBoxIt.js/css/jquery.selectBoxIt.css" />

        <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
        <script src="http://gregfranko.com/jquery.selectBoxIt.js/js/jquery.selectBoxIt.min.js"></script>
        <script src="js/stylefill.js" type="text/javascript"></script>
        <script src="js/bigtext.js" type="text/javascript"></script>    
        <style>.aam{visibility:hidden;}</style>

    </head>

    <body class="home-bg">


        <!-- TOP BANNER -->
        <div class="banner_header bannertop pink live animated">

            <div class="lg-to-sm hidden-xs">
                <div class="logo col-lg-2 col-md-2 hidden-sm">
                    <a href="#"><img alt="Logo" class="logo-img" src="img/logo.png"/></a>
                </div>

                <div class="col-lg-7 col-md-7 col-sm-9 col-xs-8 no-smrgn">
                    <p class="desc col-lg-12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur</p>
                </div>

                <div class="pull-right action-buttons col-lg-2 col-md-3 col-sm-3 col-xs-4 no-smrgn">
                    <button class="btn btn-option anim-fast no-smrgn mrgn-md"><p> Lorem ipsum dol </p></button>


                </div>
            </div>

            <div class="xs-content visible-xs">

                <div class="xs-desc col-xs-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur</div>

                <div class="col-xs-2"> 
                    <button class="xs-btn-option">
                        <i class="ion-archive"></i>
                    </button>

                </div>

            </div>
        </div>
        <!-- END TOP BANNER -->






        <div class="container">




            <div class="row topo">


                <div class="head col-xs-12 col-sm-8 col-sm-offset-2 animated fadeIn">

                    <a href="<?php echo $URL; ?>"><img alt="Logo" class="title-logo center-block small" src="<?php echo $logo; ?>"/></a>
                    <form action="create.php" method="POST" enctype="multipart/form-data">
                        <div class="input col-xs-12 col-sm-8 col-sm-offset-2">
                            <p class="input-label">URL to be shortened</p>
                            <input type="text" id="Url" class="form-control input-field anim" placeholder="Insert Long URL" autocomplete="off" required>
                            <input type="hidden" value="" name="nlink" id="th1" required>
                        </div><!-- END .col-xs-10 -->
                        <div class="col-xs-10 col-xs-offset-1">

                            <div class="intro">
                                <div class="intro-element col-sm-4 mrgn-top text-center">
                                    <p class="intro-title"><i class="ion-wand"></i></p>
                                    <br>
                                    <p class="intro-text"><strong>Create. </strong>The shortened URL is now completely yours with all the customization tools available.</p>
                                </div>
                                <div class="intro-element col-sm-4 mrgn-top text-center">
                                    <p class="intro-title"><i class="ion-paper-airplane"></i></p>
                                    <br>
                                    <p class="intro-text"><strong>Share. </strong>Expand your reach by giving out the link to other people on the planet, and have them see it!</p>
                                </div>
                                <div class="intro-element col-sm-4 mrgn-top text-center">
                                    <p class="intro-title"><i class="ion-stats-bars"></i></p>
                                    <br>
                                    <p class="intro-text"><strong>See for yourself. </strong>Offered also is a brief about the link you've generated. Who saw it, which site, and where on the planet!</p>
                                </div>
                            </div><!-- END .intro--></div>
                </div>
                <div class="col-md-6 col-md-offset-3">
                    <div class="customization">
                        <div class="input col-xs-10 col-xs-offset-1">
                            <p class="input-label">Custom alias </p>
                            <input type="text" name="short" class="form-control input-field anim" placeholder="<?php echo $URL; ?>/{ALILAS}" autocomplete="off" >
                        </div>
                        <div class="position-container col-xs-10 col-xs-offset-1">
                            <p class="input-label">Pick a position:</p>
                            <div class="col-sm-4 col-xs-12">
                                <div class="col-xs-12 anim-fast position-div tag-div selected-position">
                                    <p class="position col-xs-12">Tag</p>
                                    <p class="position-sub">(Box)</p>
                                </div>
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <div class="col-xs-12 anim-fast position-div top">
                                    <p class="position col-xs-12">Top</p>
                                    <p class="position-sub">(Banner)</p>
                                </div>
                                <div class="col-xs-12 anim-fast position-div bottom">
                                    <p class="position col-xs-12">Bottom</p>
                                    <p class="position-sub">(Banner)</p>
                                </div> 
                            </div>
                        </div>

                        <div class="color-container col-xs-10 col-xs-offset-1">
                            <p class="input-label">Pick a color:</p> <!-- .pink-tag specifies pink color for the tag only -->
                            <div title="Pink" class="col-xs-2 anim-fast color-div pink pink-div selected-color"></div>
                            <div title="Red" class="col-xs-2 anim-fast color-div red red-div"></div>
                            <div title="Yellow" class="col-xs-2 anim-fast color-div yellow yellow-div"></div>
                            <div title="Green" class="col-xs-2 anim-fast color-div green green-div"></div>
                            <div title="Blue" class="col-xs-2 anim-fast color-div blue blue-div"></div>
                            <div title="Purple" class="col-xs-2 anim-fast color-div purple purple-div"></div>
                        </div>

                        <div class="input col-xs-10 col-xs-offset-1 mrgn-top">
                            <p class="input-label">Text to be displayed</p>
                            <input type="text" name="text"  class="form-control input-field anim" placeholder="Insert text" autocomplete="off" id="text">
                        </div>

                        <div class="input col-xs-5 col-xs-offset-1 mrgn-top">
                            <p class="input-label">Button Text <small>(Optional)</small></p>
                            <input type="text" name="btnT"  class="form-control input-field anim" placeholder="Insert Button text" autocomplete="off" id="btnText">
                        </div>


                        <div class="input col-xs-5 col-xs-offset-1 mrgn-top">
                            <p class="select-label" title="Shown on small screens only, instead of the Button text!">icon <small>(For mobile)</small></p>
                            <div class="alt-icon anim download selected-alt-icon"><i class="ion-archive"></i></div>
                            <div class="alt-icon anim goto"><i class="ion-android-open"></i></div>
                            <div class="alt-icon anim buy"><i class="ion-ios-cart"></i></div>
                            <div class="alt-icon anim info"><i class="ion-information-circled"></i></div>
                        </div>
                        <div class="input col-xs-10 col-xs-offset-1 mrgn-top">
                            <p class="input-label">Button Link <small>(Optional, leave it blank to redirect to long URL)</small></p>
                            <input type="text" name="btnL" class="form-control input-field anim" placeholder="Link" autocomplete="off" >
                        </div>  
                        <input type="hidden" value="1" id="pos" name="pos">
                        <input type="hidden" value="1" id="col" name="col">
                        <input type="hidden" value="1" id="icon" name="icon">
                        <div class="col-xs-10 col-xs-offset-1">
                            <a pooname="submit" onclick="focusonshort(0)" class="btn btn-default create-btn col-xs-12 anim-fast mrgn-top">Create</a>
                            <button type="submit" role="submit" style="display:none;"></button>
                        </div>
                        </form>
                    </div> <!-- END CUSTOMIZATION DIV -->

                </div>
            </div>


        </div>








        <!-- TAG -->

        <div id="aam" class="tag tag-pink live animated">

            <div class="tag-left-content">
                <img alt="logo" class="tag-logo logo" src="img/logo.png"/>
                <p class="tag-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices eta.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices eta.</p>
                <button class="btn tag-btn-option btn-option anim-fast no-smrgn mrgn-md"><p> Option here </p></button>
            </div>


        </div>

        <!-- END TAG -->



        <!-- BOTTOM BANNER -->
        <div class="banner_header bannerbottom pink live animated">

            <div class="lg-to-sm hidden-xs" style="margin-top:-7px;">
                <div class="logo col-lg-2 col-md-2 hidden-sm">
                    <a href="#"><img alt="Logo" class="logo-img" src="img/logo.png"/></a>
                </div>

                <div class="col-lg-7 col-md-7 col-sm-9 col-xs-8 no-smrgn">
                    <p class="desc col-lg-12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur</p>
                </div>

                <div class=" action-buttons col-lg-2 col-md-3 col-sm-3 col-xs-4 no-smrgn">
                    <button class="btn btn-option anim-fast mrgn-md"><p> Option here </p></button>


                </div>
            </div>

            <div class="xs-content visible-xs" style="margin-top:-7px;">

                <div class="xs-desc col-xs-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur</div>

                <div class="col-xs-2"> 
                    <button class="xs-btn-option">
                        <i class="ion-archive"></i>

                    </button>

                </div>

            </div>
        </div>

        <!-- END BOTTOM BANNER -->
        <!--
        color classes [pink red yellow green blue purple]  
        tagColor classes [tag-pink tag-red tag-yellow tag-green tag-blue tag-purple]
        alt-icon classes [ion-archive ion-android-open ion-ios-cart ion-information-circled] -->      
        <div style="display:block;padding-top:30px;padding-bottom:30px;margin:auto;"><?php echo $ads1; ?></div>
        <script src="js/alertify.min.js"></script>

        <script>

                   function isValidUrl(url) {

                       var myVariable = url;
                       if (/^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(myVariable)) {
                           return 1;
                       } else {
                           return -1;
                       }
                   }

                   function completeUrl(url) {
                       var firstletter = url.substr(0, 7);
                       var firstletter = firstletter.toLowerCase();
                       var firstUrl = url;
                       if ((firstletter != "http://") && (firstletter != "https:/")) {
                           var newurl = "http://" + firstUrl;
                       } else {
                           var newurl = firstUrl;
                       }
                       ;
                       return newurl;
                   }
                   
                   function focusonshort(ioo) {
                       if (ioo == 1) {
                           $("input[name=short]").focus();
                       } else if (ioo == 0) {
                           var test1 = /^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test($("#th1").val());
                           if (test1) {
                                var test2 = /^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test($("input[name=btnL]").val());
                                if (test2) {
                                    $("#Url").parent("div").removeClass("has-error");
                                    $('button[type=submit]').click();
                                } else {
                                    $("input[name=btnL]").parent("div").addClass("has-error");
                                    $('input[name=btnL]').focus();
                                };
                           } else {
                               $("#Url").parent("div").addClass("has-error");
                               $('#Url').focus();
                           }
                       } else if (ioo == 2) {
                            $("input[name=btnL]").focus();
                       }
                   }
                   $(document).ready(function () {
                       $('form').on('keyup keypress', function (e) {
                           var keyCode = e.keyCode || e.which;
                           if (keyCode === 13) {
                               e.preventDefault();
                               return false;
                           }
                       });
                       $('#Url').on("keyup keypress mouseleave", function () {
                           var value = completeUrl($(this).val());
                           $("#th1").val(value);
                           var th1 = isValidUrl($("#th1").val());
                           if (th1 == 1) {
                               $(".title").css({fontSize: '50px', top: '10px'});
                               $(".intro").fadeOut(400, function () {});
                               setTimeout(function () {
                                   $('.customization').addClass("animated fadeInUp");
                                   $(".customization").show();
                               }, 700);
                           }
                           ;
                       });
                   });
                   $(document).ready(function () {
                       $(".bottom").click(function () {
                           $("#aam").addClass('aam');
                       });
                       $(".top").click(function () {
                           $("#aam").addClass('aam');
                       });
                       $(".tag-div").click(function () {
                           $("#aam").removeClass('aam');
                       });
                   });
                   $("input[name=btnL]").change(function () {
                    var btnL = completeUrl($(this).val());
                    $(this).val(btnL);
                    if (!/^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(btnL)) {
                            $("input[name=btnL]").parent("div").addClass("has-error");
                            $("a[pooname=submit]").attr('onclick', 'focusonshort(2)');
                            alertify.error("This is not a valid URL!");
                       } else {
                            $("input[name=btnL]").parent("div").removeClass("has-error");
                            $("a[pooname=submit]").attr('onclick', 'focusonshort(0)');
                       }
                   });
                   $("input[name=short]").change(function () {
                       var value = $(this).val();
                       value = value.replace(/[^a-z0-9]/gi, '');
                       $(this).val(value);
                       var focus = $(this);
                       var element = $(this).parent("div");
                       $.post("checklink.php", {chklinkjs: value}, function (value) {
                           if (value == 1) {
                               element.addClass("has-error");
                               $("a[pooname=submit]").attr('onclick', 'focusonshort(1)');
                               alertify.error("This alias is not available!");
                               focus.focus();
                           } else {
                               element.removeClass("has-error");
                               $("a[pooname=submit]").attr('onclick', 'focusonshort(0)');
                           }
                       })
                   });
                   $('#Url').focusout(function () {
                       var value = completeUrl($(this).val());
                       var element = $(this).parent("div");
                       $("#th1").val(value);
                       var th1 = isValidUrl($("#th1").val());
                       if (th1 == 1) {
                           $.post("checklink.php", {iframe: value}, function (value) {
                               if (value == 1) {
                                   element.addClass("has-warning");
                                   alertify.log("This URL does not allow iframe, Website details with a link will be used instead.");
                               } else {
                                   element.removeClass("has-warning");
                               }
                           })
                       }
                       ;
                   });
                   function increasepaddingfun(int) {
                       if ($(".bannertop").css("display") == "block") {
                           var height = $(".bannertop").height();
                           var initpadding = $(".topo").css("padding-top").replace("px", "");
                           if (initpadding < height) {
                               $(".topo").stop().animate({paddingTop: height + "px"}, 200);
                           } else if (initpadding > height) {
                               height = initpadding - height;
                               $(".topo").stop().animate({paddingTop: height + "px"}, 200);
                           }
                       } else {
                           $(".topo").stop().animate({paddingTop: 0 + "px"}, 200);
                       }
                       ;
                   }
                   $(document).click(function () {
                       increasepaddingfun();
                   })

        </script>
        <script>
            var updatePosTop = function () {
                var bannerTopHeight = $('.bannertop').height();
                var logoTopHeight = $(".bannertop .logo-img").height();
                var btnTopHeight = $(".bannertop .action-buttons").height();

                $(".bannertop .logo .logo-img").css('top', bannerTopHeight / 2 - logoTopHeight / 2 + 'px');
                $(".bannertop .action-buttons").css('top', bannerTopHeight / 2 - btnTopHeight / 2 + 'px');
            }

            var updatePosBottom = function () {
                var bannerBottomHeight = $('.bannerbottom').height();
                var logoBottomHeight = $(".bannerbottom .logo-img").height();
                var btnBottomHeight = $(".bannerbottom .action-buttons").height();

                $(".bannerbottom .logo .logo-img").css('bottom', bannerBottomHeight / 2 - logoBottomHeight / 2 + 'px');
                $(".bannerbottom .action-buttons").css('bottom', bannerBottomHeight / 2 - btnBottomHeight / 2 + 'px');
            }

            $(document).ready(function () {
                updatePosTop();
                updatePosBottom();
            });

            $("#text, #btnText").on('change mouseleave keyup keydown', function () {
                updatePosTop();
                updatePosBottom();

            });

            $(".top").on('click mouseleave mouseover mouseup', function () {
                updatePosTop();
            });

            $(".bottom").on('click mouseleave mouseover mouseup', function () {
                updatePosBottom();
            });

            $(window).on('load resize', function () {
                updatePosTop();
                updatePosBottom();

            });
        </script>
        <script>

            $(document).ready(function () {
                $(".pink-div").click(function () {
                    $('#col').val('1');
                    $(".bannertop, .bannerbottom").removeClass("pink red yellow green blue purple");
                    $(".bannertop, .bannerbottom").addClass("pink");
                    $(".tag").removeClass("tag-pink tag-red tag-yellow tag-green tag-blue tag-purple");
                    $(".tag").addClass("tag-pink");
                });

                $(".red-div").click(function () {
                    $('#col').val('2');
                    $(".bannertop, .bannerbottom").removeClass("pink red yellow green blue purple");
                    $(".bannertop, .bannerbottom").addClass("red");
                    $(".tag").removeClass("tag-pink tag-red tag-yellow tag-green tag-blue tag-purple");
                    $(".tag").addClass("tag-red");
                });

                $(".yellow-div").click(function () {
                    $('#col').val('3');
                    $(".bannertop, .bannerbottom").removeClass("pink red yellow green blue purple");
                    $(".bannertop, .bannerbottom").addClass("yellow");
                    $(".tag").removeClass("tag-pink tag-red tag-yellow tag-green tag-blue tag-purple");
                    $(".tag").addClass("tag-yellow");
                });

                $(".green-div").click(function () {
                    $('#col').val('4');
                    $(".bannertop, .bannerbottom").removeClass("pink red yellow green blue purple");
                    $(".bannertop, .bannerbottom").addClass("green");
                    $(".tag").removeClass("tag-pink tag-red tag-yellow tag-green tag-blue tag-purple");
                    $(".tag").addClass("tag-green");
                });

                $(".blue-div").click(function () {
                    $('#col').val('5');
                    $(".bannertop, .bannerbottom").removeClass("pink red yellow green blue purple");
                    $(".bannertop, .bannerbottom").addClass("blue");
                    $(".tag").removeClass("tag-pink tag-red tag-yellow tag-green tag-blue tag-purple");
                    $(".tag").addClass("tag-blue");
                });

                $(".purple-div").click(function () {
                    $('#col').val('6');
                    $(".bannertop, .bannerbottom").removeClass("pink red yellow green blue purple");
                    $(".bannertop, .bannerbottom").addClass("purple");
                    $(".tag").removeClass("tag-pink tag-red tag-yellow tag-green tag-blue tag-purple");
                    $(".tag").addClass("tag-purple");
                });

                $(".download").click(function () {
                    $('#icon').val('1');
                    $(".xs-btn-option>i").removeClass("ion-archive ion-android-open ion-ios-cart ion-information-circled");
                    $(".xs-btn-option>i").addClass("ion-archive");
                });

                $(".goto").click(function () {
                    $('#icon').val('2');
                    $(".xs-btn-option>i").removeClass("ion-archive ion-android-open ion-ios-cart ion-information-circled");
                    $(".xs-btn-option>i").addClass("ion-android-open");
                });

                $(".buy").click(function () {
                    $('#icon').val('3');
                    $(".xs-btn-option>i").removeClass("ion-archive ion-android-open ion-ios-cart ion-information-circled");
                    $(".xs-btn-option>i").addClass("ion-ios-cart");
                });


                $(".info").click(function () {
                    $('#icon').val('4');
                    $(".xs-btn-option>i").removeClass("ion-archive ion-android-open ion-ios-cart ion-information-circled");
                    $(".xs-btn-option>i").addClass("ion-information-circled");
                });

                $("#btnText").on('change click', function () {
                    $(".btn-option, .tag-btn-option").fadeIn(150, function () {});
                });

                $(".alt-icon").on("click", function () {
                    $(".xs-btn-option").fadeIn(150, function () {});
                });

            });
        </script>
        <script>
            $(document).ready(function () {

                // For color selection
                $(".color-div").click(function () {
                    $(this).addClass('selected-color');
                    $(".color-div").not(this).removeClass('selected-color');
                });

                // For position selection
                $(".position-div").click(function () {
                    $(this).addClass('selected-position');
                    $(".position-div").not(this).removeClass('selected-position');
                });

                // For alt-icon selection
                $(".alt-icon").click(function () {
                    $(this).addClass('selected-alt-icon');
                    $(".alt-icon").not(this).removeClass('selected-alt-icon');
                });

            });

        </script>
        <script>

            $(document).ready(function () {
                $(".top").click(function () {
                    updatePosTop();
                    $('#pos').val('2');
                    $(".bannertop").show();
                    $(".title").css('margin-top', $('.banner_header').height() + 'px');
                    $("#text").keyup(function () {
                        $(".title").css('margin-top', $('.banner_header').height() + 'px');
                        increasepaddingfun();

                    });
                    $(".bannerbottom, .tag").hide();
                    $(".container").css('padding-bottom', '10px');
                });
            });

            $(document).ready(function () {
                $(".close-top").click(function () {
                    $(".bannertop").hide();
                    $(".title").css('margin-top', '15px');
                    $(".container").css('padding-bottom', '10px');
                });
            });

            $(document).ready(function () {
                $(".bottom").click(function () {
                    updatePosBottom();
                    $('#pos').val('3');
                    $(".bannerbottom").show();
                    $(".bannertop, .tag").hide();
                    $(".title").css('margin-top', '15px');
                    $(".container").css('padding-bottom', $('.bannerbottom').height() + 15 + 'px');
                    $("#text").keyup(function () {
                        $(".container").css('padding-bottom', $('.bannerbottom').height() + 15 + 'px');
                    });
                });
            });

            $(document).ready(function () {
                $(".close-bottom").click(function () {
                    $(".bannerbottom").hide();
                    $(".title").css('margin-top', '15px');
                    $(".container").css('padding-bottom', '10px');
                });
            });

            $(document).ready(function () {
                $(".tag-div").click(function () {
                    $('#pos').val('1');
                    $(".tag").show();
                    $(".bannertop, .bannerbottom").hide();
                    $(".title").css('margin-top', '15px');
                    $(".container").css('padding-bottom', $('.tag').height() + 50 + 'px');
                    $("#text").keyup(function () {
                        $(".container").css('padding-bottom', $('.tag').height() + 50 + 'px');
                    });
                    setTimeout(function () {
                        $(".tag").fadeTo(3000, 0.4, function () {});
                    }, 1000);
                    $('.tag').on('mouseover', function () {
                        $(".tag").stop().fadeTo(200, 1, function () {});
                    });
                    $('.tag').on('mouseleave', function () {
                        $(".tag").finish();
                        $(".tag").fadeTo(200, 1, function () {});
                        setTimeout(function () {
                            $(".tag").fadeTo(5000, 0.4, function () {});
                        }, 500);
                    });
                });
            });
            $(document).ready(function () {
                $(".tag-close").click(function () {
                    $(".tag").hide();
                    $(".title").css('margin-top', '15px');
                    $(".container").css('padding-bottom', '10px');
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $(".desc, .tag-desc").text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur");

                $("#text").on('keyup mouseleave', function () {
                    var text = $(this).val();
                    $(".desc, .tag-desc, .xs-desc").text(text);
                }).keyup();

                $("#btnText").on('keyup mouseleave', function () {
                    var btnText = $(this).val();
                    $(".btn-option").text(btnText);
                }).keyup();

            });
        </script>
        <script>
            $('.desc, .xs-desc, .position, .title').bigtext();
            $('.desc, .xs-desc, .position').bigtext({
                maxfontsize: 25
            });
            $('.desc, .xs-desc, .position').bigtext({
                minfontsize: 12
            });
            $('.title').bigtext({
                minfontsize: 50
            });
        </script>
        <?php echo $results["track"]; ?>
    </body>
</html>
