<?php $db->close_connection(); ?>

<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">


        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <title><?php echo $name . ' - ' . $mytitle; ?></title>
        <meta name="description" content="<?php echo $text; ?>">



        <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />

        <!--- Font-Awesome CDN -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!--- Core CSS-->
        <link href="css/normalize.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/style.css" media="all" rel="stylesheet" type="text/css" />

        <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/stylefill.js" type="text/javascript"></script>
        <script src="js/minmaxfontsize.js" type="text/javascript"></script>
        <script>
            //function to fix height of iframe!
            var calcHeight = function () {
                var headerDimensions = $('.banner_header').height();
                $('.iframe').height($(window).height() - headerDimensions - 0);
            }

            $(document).ready(function () {
                calcHeight();
            });

            $(window).on('load resize', function () {
                calcHeight();
            });


        </script>
        <script>
            var updatePosTop = function () {
                var bannerTopHeight = $('.bannertop').height();
                var logoTopHeight = $(".bannertop .logo-img").height();
                var btnTopHeight = $(".bannertop .action-buttons").height();

                $(".bannertop .logo .logo-img").css('top', bannerTopHeight / 2 - logoTopHeight / 2 + 'px');
                $(".bannertop .action-buttons").css('top', bannerTopHeight / 2 - btnTopHeight / 2 + 'px');
            }
            $(document).ready(function () {
                updatePosTop();

            });

            $(window).on('load resize', function () {
                updatePosTop();

            });

        </script>

    </head>

    <body class="full-screen-preview" style="background-color:#e8e8e8">

        <!--- Banner Code -->
        <div class="banner_header bannertop  <?php echo $color; ?>">

            <div class="lg-to-sm hidden-xs">
                <div class="logo col-lg-2 col-md-2 hidden-sm">
                    <a href="<?php echo $URL; ?>"><img alt="Logo" class="logo-img" src="<?php echo $logo; ?>"/></a>
                </div>

                <div class="col-lg-7 col-md-7 col-sm-9 col-xs-8 no-smrgn">
                    <p class="desc col-lg-12"><?php echo $text; ?></p>
                </div>

                <div class="pull-right action-buttons col-lg-2 col-md-3 col-sm-3 col-xs-4 no-smrgn">
                    <?php
                    if ($button == !'') {
                        if ($btnL == '') {
                            $btnL = $link;
                        }
                        ?>
                        <a href="button.php?id=<?php echo $shr; ?>"><button class="btn btn-option anim-fast no-smrgn mrgn-md"><p> <?php echo $button; ?> </p></button></a>
                        <?php
                    } else {
                        
                    }
                    ?>
                </div>
            </div>

            <div class="xs-content visible-xs">

                <div class="xs-desc col-xs-10"><?php echo $text; ?></div>
                <div class="col-xs-2"> 
                    <?php
                    if ($button == !'') {
                        if ($btnL == '') {
                            $btnL = $link;
                        }
                        ?>
                        <a href="<?php echo $btnL; ?>"><button class="xs-btn-option">
                                <i class="<?php echo $icon; ?>"></i>

                            </button>
                        </a>
                        <?php
                    } else {
                        
                    }
                    ?>
                </div>

            </div>
        </div>

    </div>

    <!--- iframe Code-->

    <?php echo $linko; ?>
    <div style=""><?php echo $ads2; ?></div>

    <script type="text/javascript">

    </script>
    <?php echo $results["track"]; ?>

</body>
</html>
