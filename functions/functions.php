<?php

function validate_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function arrToStr($array) {
    $newArray = $array;
    $newArray[0] = implode("_", $newArray[0]);
    $newArray[1] = implode("_", $newArray[1]);
    $newArray = implode("&", $newArray);
    return $newArray;
}

function strToArr($array) {
    $newArray = explode("&", $array);
    $newArray[0] = explode("_", $newArray[0]);
    $newArray[1] = explode("_", $newArray[1]);
    return $newArray;
}

function getUserIP() {
    $client = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote = $_SERVER['REMOTE_ADDR'];

    if (filter_var($client, FILTER_VALIDATE_IP)) {
        $ip = $client;
    } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
        $ip = $forward;
    } else {
        $ip = $remote;
    }

    return $ip;
}

function locateIP($ip) {
    if (preg_match("/\b(?:(?:2(?:[0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9])\.){3}(?:(?:2([0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9]))\b/i", $ip)) {
        $ip1 = $ip;
    } else {
        return "US";
    };
    $location = json_decode(file_get_contents("http://ip-api.com/json/" . $ip1), true);
    $location = $location['countryCode'];
    return $location;
}

;
?>