<?php $db->close_connection(); ?>
<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">


        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <title><?php echo $name . ' - ' . $mytitle; ?></title>
        <meta name="description" content="<?php echo $text; ?>">

        <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />

        <!--- Font-Awesome CDN -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

        <!--- Core CSS-->
        <link href="css/normalize.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/style.css" media="all" rel="stylesheet" type="text/css" />

        <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/stylefill.js" type="text/javascript"></script>
        <script src="js/minmaxfontsize.js" type="text/javascript"></script>

        <script>
            //function to fix height of iframe!
            var calcHeight = function () {
                var headerDimensions = $('.banner_header').height();
                $('.iframe').height($(window).height() - headerDimensions - 7);
            }

            $(document).ready(function () {
                calcHeight();
            });

            $(window).on('load resize', function () {
                calcHeight();
            });
        </script>
        <script>

            var updatePosBottom = function () {
                var bannerBottomHeight = $('.bannerbottom').height();
                var logoBottomHeight = $(".bannerbottom .logo-img").height();
                var btnBottomHeight = $(".bannerbottom .action-buttons").height();

                $(".bannerbottom .logo .logo-img").css('bottom', bannerBottomHeight / 2 - logoBottomHeight / 2 + 'px');
                $(".bannerbottom .action-buttons").css('bottom', bannerBottomHeight / 2 - btnBottomHeight / 2 + 'px');
            }

            $(document).ready(function () {
                updatePosBottom();
            });

            $(window).on('load resize', function () {
                updatePosBottom();

            });

        </script>



    </head>

    <body class="full-screen-preview <?php echo $color; ?>">

        <?php echo $linko; ?>
        <div style="position: fixed;text-align:center;z-index:1;top:5vh;left:10vw;"><?php echo $ads2; ?></div>
        <div style="position: fixed;text-align:center;z-index:1;top:5vh;right:10vw;"><?php echo $ads2; ?></div>
        <!--- Banner Code -->
        <div class="banner_header bannerbottom <?php echo $color; ?>" style="padding-top:10px;">

            <div class="lg-to-sm hidden-xs" style="margin-top:-7px;">
                <div class="logo col-lg-2 col-md-2 hidden-sm">
                    <a href="<?php echo $URL; ?>"><img alt="Logo" class="logo-img" src="<?php echo $logo; ?>"/></a>
                </div>

                <div class="col-lg-7 col-md-7 col-sm-9 col-xs-8 no-smrgn">
                    <p class="desc col-lg-12"><?php echo $text; ?></p>
                </div>

                <div class="pull-right action-buttons col-lg-2 col-md-3 col-sm-3 col-xs-4 no-smrgn">
                    <?php
                    if ($button == !'') {
                        if ($btnL == '') {
                            $btnL = $link;
                        }
                        ?>
                        <a href="button.php?id=<?php echo $shr; ?>"><button class="btn btn-option anim-fast no-smrgn mrgn-md"><p> <?php echo $button; ?> </p></button></a>
                        <?php
                    } else {
                        
                    }
                    ?>

                </div>
            </div>

            <div class="xs-content visible-xs" style="margin-top:-7px;">

                <div class="xs-desc col-xs-10"><?php echo $text; ?></div>

                <div class="col-xs-2"> 
                    <?php
                    if ($button == !'') {
                        if ($btnL == '') {
                            $btnL = $link;
                        }
                        ?>
                        <a href="<?php echo $btnL; ?>"><button class="xs-btn-option">
                                <i class="<?php echo $icon; ?>"></i>

                            </button>
                        </a>
                        <?php
                    } else {
                        
                    }
                    ?>

                </div>

            </div>
        </div>

    </div>

    <?php echo $results["track"]; ?>
</body>
</html>
