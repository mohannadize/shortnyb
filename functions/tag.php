<?php $db->close_connection(); ?>

<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">


        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <title><?php echo $name . ' - ' . $mytitle; ?> </title>
        <meta name="description" content="<?php echo $text; ?>">

        <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />

        <!--- Font-Awesome CDN -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

        <!--- Custom Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Montserrat'>
        <link href='http://fonts.googleapis.com/css?family=Abel'>
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400'>

        <!--- Core CSS-->
        <link href="css/normalize.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
        <link href="css/style.css" media="all" rel="stylesheet" type="text/css" />

        <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/stylefill.js" type="text/javascript"></script>
        <script src="js/minmaxfontsize.js" type="text/javascript"></script>

        <script>
            $(document).ready(function () {
                $(".close").on('click', function () {
                    $(".tag").hide();
                });

            });
        </script>

        <style> 
            html {overflow: auto;} 
            html, body, div, iframe {margin: 0px; padding: 0px; height: 100%; border: none;} 
            iframe {display: block; width: 100%; border: none; overflow-y: auto; overflow-x: hidden; background-color:#fff;} 
        </style> 

    </head>

    <body class="" style="height:100%;">

        <!--- iframe Code-->
        <?php echo $linko; ?>
        <div style=""><?php echo $ads2; ?></div>
        <div class="tag tag-<?php echo $color; ?>">

            <div class=""> <!--- Visible when width>768px--> 
                <div class="tag-left-content">
                    <a href="<?php echo $URL; ?>"><img alt="logo" class="tag-logo logo" src="<?php echo $logo; ?>"/></a>
                    <p class="tag-desc"><?php echo $text; ?></p>
                    <?php
                    if ($button == !'') {
                        if ($btnL == '') {
                            $btnL = $link;
                        }
                        ?>
                        <a href="button.php?id=<?php echo $shr; ?>"><button class="btn tag-btn-option btn-option anim-fast no-smrgn mrgn-md"><p> <?php echo $button; ?> </p></button></a>
                        <?php
                    } else {
                        
                    }
                    ?>
                </div>

<!-- <a href="tawela.html" class="close anim-xfast pull-right tag-close"><i class="fa fa-close"></i></a> -->

            </div>

        </div>
        <script>
            $(document).ready(function () {

                setTimeout(function () {
                    $(".tag").fadeTo(3000, 0.4, function () {});
                }, 1000);
                $('.tag').on('mouseover', function () {
                    $(".tag").stop().fadeTo(200, 1, function () {});
                });
                $('.tag').on('mouseleave', function () {
                    $(".tag").finish();
                    $(".tag").fadeTo(200, 1, function () {});
                    setTimeout(function () {
                        $(".tag").fadeTo(5000, 0.4, function () {});
                    }, 500);
                });
            });
        </script>

        <?php echo $results["track"]; ?>

    </body>
</html>
